ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.010591, 83.049625],
            zoom: 15.5,
            controls: []
        });
        myMap.behaviors.disable('scrollZoom');
        
        var myPlacemark = new ymaps.Placemark([55.010591, 83.049625], {}, {
                preset: 'twirl#redIcon' 
            });
     
        myMap.geoObjects.add(myPlacemark); // Размещение геообъекта на карте.
  //  YMaps.Events.observe(function () {
 //       map.removeControl(typecontrol);
 //   });
});
