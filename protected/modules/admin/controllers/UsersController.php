<?php

class UsersController extends AdminController
{
    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionIndex()
    {
        $model = new Users('search');
        $model->unsetAttributes();
        if ( isset($_GET['Users']) ) {
            $model->attributes=$_GET['Users'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Users::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    
    public function actionDownloadZipFile($unicode,$blank_1,$blank_2,$blank_3,$blank_4,$blank_5,$blank_6,$blank_7,$blank_8)
        { 
            $zip = new ZipArchive;
            $fileName = $unicode.".zip";
            $zip->open($fileName, ZIPARCHIVE::CREATE);
            
            if( !empty($blank_1) ){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_1,$blank_1);   
            }
            if( !empty($blank_2) ){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_2, $blank_2);
            }
            if( !empty($blank_3) ){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_3, $blank_3);
            }
            if( $blank_4 != NULL){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_4, $blank_4);
            }
            if( $blank_5 != NULL){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_5, $blank_5);
            }
            if( $blank_6 != NULL){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_6, $blank_6);
            }
            if( $blank_7 != NULL){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_7, $blank_7);
            }
            if( $blank_8 != NULL){
                $zip->addFile($_SERVER['DOCUMENT_ROOT']."/uploads/doc/".$blank_8, $blank_8);
            }
            //$zip->addFile("index.php");
            $zip->close();
            $file = $fileName; // " ./img "
            // отдаем файл
            Yii::app()->request->sendFile(basename($file),file_get_contents($file));
        }
        
        
        
    public function actionZip(){
        $zip = new ZipArchive;
        if ($zip->open('test.zip') === TRUE) {
            $zip->addFromString('dir/test.txt', 'здесь следует содержимое файла');
            $zip->close();
            echo 'ok';
        } else {
            echo 'failed';
        }
    }
}
