<?php

class SliderController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Slider();
        
        if ( isset($_POST['Slider']) ) {
            $model->attributes = $_POST['Slider'];
            $model->date = strtotime($model->date);
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/slider/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/slider/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){
                    
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $model->date=date('d.m.Y',$model->date);
        if ( isset($_POST['Slider']) ) {
            $model->attributes = $_POST['Slider'];
            $model->date = strtotime($model->date);
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/slider/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/slider/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Slider('search');
        $model->unsetAttributes();
        if ( isset($_GET['Slider']) ) {
            $model->attributes=$_GET['Slider'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Slider::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
