<?php echo BsHtml::pageHeader('Заказы') ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'name' => 'datePayment',
            'value' => 'Yii::app()->dateFormatter->format(\'HH:mm d MMM yyyy г.\', $data->datePayment)',
            'filter' => false,
        ),
        array(
            'name'=>'email',
        ),
        array(
            'name'=>'name',
        ),
        array(
            'name'=>'phone',
        ),
        array(
            'name'=>'type',
            'value'=>'$data->Type'
        ),
        array(
            'name'=>'delivery',
            'value'=>'$data->Delivery'
        ),
        array(
            'name'=>'isPayment',
            'value'=>'$data->Payment'
        ),
        array(
            'name'=>'price',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete} {zip}',
            'buttons'=>array(
                'zip' => array
                (
                    'label'=>'Скачать архив',
                    'icon'=>'plus',
                    'url'=>'Yii::app()->createUrl("admin/users/DownloadZipFile", array("unicode"=>$data->unicode,"blank_1"=>$data->blank_1,"blank_2"=>$data->blank_2,"blank_3"=>$data->blank_3,"blank_4"=>$data->blank_4,"blank_5"=>$data->blank_5,"blank_6"=>$data->blank_6,"blank_7"=>$data->blank_7,"blank_8"=>$data->blank_8))',
                    'options'=>array(
                        'class'=>'btn',
                    ),
                ),
            ),
        ),
    ),
)); ?>