<?php
    $this->pageTitle = Yii::app()->name.' - '.'Конфигурация системы';
?>
<?php $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => 'Конфигурация системы',
)); ?>

    <?php if ( Yii::app()->user->getFlash('success') ): ?>
        <?= BsHtml::alert(BsHtml::ALERT_COLOR_INFO, 'Информация обновлена.') ?>
    <?php endif; ?>

    <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'config-form',
        'enableAjaxValidation'=>false,
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnChange'=>true,
            'validateOnSubmit'=>true,
        ),
    )); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->textFieldControlGroup($model,'adminEmail',array(
            'maxlength'=>64,
        )); ?>

        <?= $form->textFieldControlGroup($model,'title'); ?>
        <?= $form->textFieldControlGroup($model,'meta_description'); ?>
        <?= $form->textFieldControlGroup($model,'meta_keys'); ?>

        <?= $form->textFieldControlGroup($model,'price_1'); ?>
        <?= $form->textFieldControlGroup($model,'price_2'); ?>
        <?= $form->textFieldControlGroup($model,'price_3'); ?>
        <?= $form->textFieldControlGroup($model,'price_4'); ?>
        <?= $form->textFieldControlGroup($model,'price_5'); ?>
        <?= $form->textFieldControlGroup($model,'price_6'); ?>

        <?= BsHtml::formActions(array(
            BsHtml::resetButton('Сброс', array(
                'color' => BsHtml::BUTTON_COLOR_WARNING,
                'icon' => BsHtml::GLYPHICON_REFRESH,
            )),
            BsHtml::submitButton('Готово', array(
                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                'icon' => BsHtml::GLYPHICON_OK,
            )),
        ), array('class'=>'form-actions')); ?>

    <?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>