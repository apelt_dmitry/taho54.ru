<?php
    $criteria = new CDbCriteria();
    $criteria->select = array('id', 'preview', 'title', 'description', 'date');
    $criteria->order = '`id` DESC';
    $criteria->limit =3;
    $lastNews = News::model()->findAll($criteria);
?>
<div class="books">
    <?php 
        mb_internal_encoding("UTF-8");
        foreach ( $lastNews as $row ): 
    ?>
    <div class="book item_1">
        <a href="/site/news#news<?= $row->id ?>"><img src="/uploads/news/preview/<?= $row->preview; ?>"></a>
        <p class="title"><a href="/site/news#news<?= $row->id ?>"><?= $row->title; ?></a></p>
        <p class="author"><?= Yii::app()->dateFormatter->format('d MMM yyyy г.', $row->date); ?></p>
        <div class="short">
            <p><span><?= mb_substr($row->description, 0, 300); ?>...</span></p>
        </div>
        <div class="clear"></div>
    </div>
    <?php endforeach; ?>
</div>
<div class="clear"></div>
<p class="show_all"><a href="/site/news">Посмотреть остальные »</a></p>