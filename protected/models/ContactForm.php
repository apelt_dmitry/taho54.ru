<?php

class ContactForm extends CFormModel
{
    public $name;
    public $phone;
    public $email;



    public function rules()
    {
        return array(
            array('name, phone','required','message'=>'Необходимо заполнить поле.'),
            array('name','length', 'max'=>64),
            array('email', 'email'),
            array('phone, email','length', 'max'=>16),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
        );
    }


    public function attributeLabels()
    {
        return array(
            'name'=>'Контактное лицо',
            'phone'=>'Телефон',
            'email' =>'Email'
        );
    }
    
    public function goContact()
    {   
        include_once "libmail.php";
        $m = new Mail;
        //$m->From($this->email.';admin@tahometr.com');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $m->Body('Заказ выезда специалиста.'.'<br/>'.'Отправитель: '.$this->name.'<br/>'.'. Телефон: '.$this->phone.'Почта: '.$this->email);
        $m->Send();
    }
}
