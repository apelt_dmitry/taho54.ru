<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property integer $type
 * @property integer $step
 * @property string $unicode
 * @property string $blank_1
 * @property string $blank_2
 * @property string $blank_3
 * @property string $blank_4
 * @property string $blank_5
 * @property string $blank_6
 * @property string $blank_7
 * @property string $blank_8
 * @property integer $price
 * @property integer $isPayment
 * @property integer $datePayment
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, step, unicode, price', 'required'),
			array('type, step, price, isPayment, datePayment, delivery', 'numerical', 'integerOnly'=>true),
                        array('name, phone, email', 'length', 'max'=>32),
                        array('email', 'email'),
			array('unicode, blank_1, blank_2, blank_3, blank_4, blank_5, blank_6, blank_7, blank_8', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, step, unicode, blank_1, blank_2, blank_3, blank_4, blank_5, blank_6, blank_7, blank_8, price, isPayment, datePayment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Тип карты',
			'step' => 'Step',
                        'name' => ' Имя',
                        'phone' => ' Телефон',
                        'email' => ' email',
			'unicode' => 'Unicode',
			'blank_1' => 'Blank 1',
			'blank_2' => 'Blank 2',
			'blank_3' => 'Blank 3',
			'blank_4' => 'Blank 4',
			'blank_5' => 'Blank 5',
			'blank_6' => 'Blank 6',
			'blank_7' => 'Blank 7',
			'blank_8' => 'Blank 8',
                        'delivery' => 'Доставка',
			'price' => 'Цена',
			'isPayment' => 'Оплачено!',
			'datePayment' => 'Дата платежа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('step',$this->step);
		$criteria->compare('unicode',$this->unicode,true);
		$criteria->compare('blank_1',$this->blank_1,true);
		$criteria->compare('blank_2',$this->blank_2,true);
		$criteria->compare('blank_3',$this->blank_3,true);
		$criteria->compare('blank_4',$this->blank_4,true);
		$criteria->compare('blank_5',$this->blank_5,true);
		$criteria->compare('blank_6',$this->blank_6,true);
		$criteria->compare('blank_7',$this->blank_7,true);
		$criteria->compare('blank_8',$this->blank_8,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('isPayment',$this->isPayment);
		$criteria->compare('datePayment',$this->datePayment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getDelivery(){
            switch ($this->delivery) {
                case 0:
                    return 'Нет';
                case 1:
                    return 'Да';
            }
        }
        public function getPayment(){
            switch ($this->isPayment) {
                case 0:
                    return 'Нет';
                case 1:
                    return 'Да';
            }
        }
        
        public function getType(){
            switch ($this->type) {
                case 1:
                    return 'ТР';
                case 2:
                    return 'СКЗИ';
                case 3:
                    return 'ЕСТР';
                case 4:
                    return 'ТР КОМПАНИИ';
                case 5:
                    return 'СКЗИ КОМПАНИИ';
                case 6:
                   return 'ЕСТР КОМПАНИИ';
            }
        }
}
