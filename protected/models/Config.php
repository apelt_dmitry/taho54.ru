<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property integer $price_1
 * @property integer $price_2
 * @property integer $price_3
 * @property integer $price_4
 * @property integer $price_5
 * @property integer $price_6
 * @property string $adminEmail
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keys
 */
class Config extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adminEmail, title, meta_description, meta_keys', 'required'),
			array('price_1, price_2, price_3, price_4, price_5, price_6', 'numerical', 'integerOnly'=>true),
			array('adminEmail', 'length', 'max'=>64),
			array('title, meta_description, meta_keys', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, price_1, price_2, price_3, price_4, price_5, price_6, adminEmail, title, meta_description, meta_keys', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'price_1' => 'Цена на карту тр',
			'price_2' => 'Цена на карту скзи',
			'price_3' => 'Цена на карту естр',
			'price_4' => 'Цена на карту компании тр',
			'price_5' => 'Цена на карту компании скзи',
			'price_6' => 'Цена на карту компании естр',
			'adminEmail' => 'Admin Email',
			'title' => 'Title',
			'meta_description' => 'Meta Description',
			'meta_keys' => 'Meta Keys',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('price_1',$this->price_1);
		$criteria->compare('price_2',$this->price_2);
		$criteria->compare('price_3',$this->price_3);
		$criteria->compare('price_4',$this->price_4);
		$criteria->compare('price_5',$this->price_5);
		$criteria->compare('price_6',$this->price_6);
		$criteria->compare('adminEmail',$this->adminEmail,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keys',$this->meta_keys,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
