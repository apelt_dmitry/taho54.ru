<?php

class OrderForm extends CFormModel 
{
    public $name;
    public $phone;
    public $email;



    public function rules()
    {
        return array(
            array('name, phone, email','required'),
            array('name,  email', 'length', 'max'=>64),
            array('email','email'),
            array('phone', 'length', 'max'=>16),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
            );
        
        
    }

    public function attributeLabels()
    {
        return array(
            'name'=>'Имя',
            'phone1'=>'Телефон',
            'email'=>'E-mail',
            );
    }
    
    public function goOrder()
    {
        include_once "libmail.php";
        $m = new Mail;
        $m->From($this->email.';admin@azimut55.ru');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $m->Body(''
                . 'Отправитель: '.$this->name.'. '
                . 'Телефон: '.$this->phone. '. '
                . 'E-mail: '.$this->email. '. '
                );
        $m->Send();
    }
    
    
}
