<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Tahometr',
    
        'sourceLanguage' => 'ru_RU',
        'language' => 'ru',
    
        'aliases' => array(
            'bootstrap' => 'ext.bootstrap',
        ),

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
                'application.helpers.*',
		'application.models.*',
		'application.components.*',
                'bootstrap.behaviors.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'admin',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                    'generatorPaths' => array('bootstrap.gii'),
		),
		
	),

	// application components
	'components'=>array(
            'robokassa' => array(
                'class' => 'application.components.Robokassa',
                'sMerchantLogin' => 'KartyVoditelya',
                'sMerchantPass1' => 'qfjriv2858',
                'sMerchantPass2' => 'qfjriv2858qfjriv2858',
                'sCulture' => 'ru',
                'sIncCurrLabel' => '',
                'orderModel' => 'Users', // ваша модель для выставления счетов
                'priceField' => 'price', // атрибут модели, где хранится сумма
                'isTest' => false, // тестовый либо боевой режим работы
            ),
            'image'=>array(
                'class'=>'application.extensions.image.CImageComponent',
                'driver'=>'GD',
                'params'=>array('directory'=>'/opt/local/bin'),
            ),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        'class' => 'bootstrap.components.BsApi',

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				//'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				//'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=>false,
		),
		

		// database settings are configured in database.php
		'db'=>array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=tahometr',
            'username' => 'root',
            'password' => '',
   //         'connectionString' => 'mysql:host=localhost;dbname=dev-d.mir-it.info',
 //           'username' => 'dev-d',
//            'password' => 'FsSMNryEwCMk',
  //          'connectionString' => 'mysql:host=localhost;dbname=u0072831_d',
  //          'username' => 'u0072831_d',
  //          'password' => '3c6qca2Y',
            'charset' => 'utf8',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
