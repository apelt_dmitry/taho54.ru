<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    //preLoader
    ->registerCssFile($pt.'css/preLoader.css')
    //slider
    //->registerCssFile($pt.'css/slider/superslides.css')
    ->registerCssFile($pt.'css/slider/style.css')
    ->registerCssFile($pt.'css/slider/custom.css')
    ->registerCssFile($pt.'css/slider/demo.css')
    ->registerCssFile($pt.'css/slider/slick.css')
    ->registerCssFile($pt.'css/slider/slick-theme.css')
        
    ->registerCssFile($pt.'css/main.css');

$cs
    
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/jquery-1.8.2.min.js',CClientScript::POS_END)
    //slider
    ->registerScriptFile($pt.'js/slider/modernizr.custom.79639.js',CClientScript::POS_END) 
    ->registerScriptFile($pt.'js/slider/jquery.ba-cond.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/slider/jquery.slitslider.js',CClientScript::POS_END)  
    ->registerScriptFile($pt.'js/slider/slick.js',CClientScript::POS_END)  
    //uploader
    ->registerScriptFile($pt.'js/jquery.iframe-transport.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/jquery.fileupload.js',CClientScript::POS_END)
    //scrollTo
    ->registerScriptFile($pt.'js/jquery.scrollTo-1.4.3.1.js',CClientScript::POS_END)
        
    ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END);

    $config=$this->config;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="<?= $config->meta_keys ?>">
        <meta name="title" content="<?= $config->title ?>">
        <meta name="description" content="<?= $config->meta_description ?>">
        <title><?php echo $this->pageTitle; ?></title>
        <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
    </head>
    <body>
        <!-- прелоадер начало -->
        <script>
            $(document).ready(function () {
                $('#loader-wrapper').animate({
                    opacity: 0
                }, 1000, null, function () {
                    $(this).hide();
                });
                
               $('.single-item').slick({
                    dots: false,
                    infinite: true,
                    speed: 500,
                    autoplay: true,
                    autoplaySpeed: 6000,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
                $('.otzivi').slick({
                    arrows: false,
                    //fade: true,
                    dots: true,
                    infinite: true,
                    speed: 500,
                    autoplay: true,
                    autoplaySpeed: 6000,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
            });
        </script>
        <!-- прелоадер конец -->
        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>
        <div class="header-line">
            <div class="container">
                <div class="logo">
                </div>
                <div class="header-text">
                    Продажа, установка и ремонт  тахографов в Новосибирске<br/>
                    <span style="font-size: 18px">Время работы пн.-пт. 08-20 сб. 10-19</span>
                </div>
                <div class="phone-header">
                    <image style="  float: left; margin: 7px; "src="../img/phone.png"/>
                    <div class="phone" >
                        <span style="font-size: 18px">+7 </span>(383) 3-109-110
                    </div>
                    <div class="zvonok">
                        Заказать звонок 
                    </div>
                </div>
            </div>
            <div class="header-border">
                
            </div>
            <div class="container">
                <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'id'=>'main_menu',
                        'items'=>array(
                            array('label'=>'Главная', 'url'=>array('site/index'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Оформить карту онлайн', 'url'=>array('site/map_selection'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'О компании', 'url'=>array('site/about_company'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Наши услуги', 'url'=>array('site/services'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Каталог', 'url'=>array('site/catalog'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Контакты', 'url'=>array('site/contacts'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Вопрос - ответ', 'url'=>array('site/faq'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Статьи', 'url'=>array('site/news'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Штрафы', 'url'=>array('site/forfeit'),'itemOptions'=> array('class'=>'li_menu')),
                        ),
                        'htmlOptions'=>array(
                            'class'=>'',
                            
                        ),
                        'activateParents'=>true,
                        'encodeLabel'=>false,
                    ));
                ?>
            </div>
        </div>
        
        <?= $content ?>
        <div style="width: 100%; position: relative;">
            <div class="main_footer">
            <div class="container">
                <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'id'=>'footer_menu',
                        'items'=>array(
                            array('label'=>'Главная', 'url'=>array('site/index'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Оформить карту онлайн', 'url'=>array('site/map_selection'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'О компании', 'url'=>array('site/about_company'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Наши услуги', 'url'=>array('site/services'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Каталог', 'url'=>array('site/catalog'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Контакты', 'url'=>array('site/contacts'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Вопрос - ответ', 'url'=>array('site/faq'),'itemOptions'=> array('class'=>'li_menu')),                            
                            array('label'=>'Штрафы', 'url'=>array('site/forfeit'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Статьи', 'url'=>array('site/news'),'itemOptions'=> array('class'=>'li_menu')),
                        ),
                        'htmlOptions'=>array(
                            'class'=>'',                            
                        ),
                        'activateParents'=>true,
                        'encodeLabel'=>false,
                    ));
                ?>
            </div>
            </div>
            <div class="main_footer_middle">
                <div class="container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2287.859837778037!2d83.05016471296693!3d55.01062193477602!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x42dfe93caeb8a589%3A0x3c1d9631976ac05f!2z0YPQuy4g0JLRi9Cx0L7RgNC90LDRjywgMTk5LzIsINCd0L7QstC-0YHQuNCx0LjRgNGB0LosINCd0L7QstC-0YHQuNCx0LjRgNGB0LrQsNGPINC-0LHQuy4sIDYzMDEyNg!5e0!3m2!1sru!2sru!4v1437584437236" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <img class="footer_img" src="/img/footer_img.png">
                    <div class="footer_info_box">
                        <span>Наш адрес:</span><br/>
                        ул. Выборная 199/2, <br/>
                        <span2 style="margin-bottom: 7px">компания "ТИМФОРС"</span2><br/>
                        <span>Телефоны:</span><br/>
                        8 (383) 3-109-110,<br/>
                        <span2 style="margin-bottom: 7px">8 (383) 331-15-50</span2><br/>
                        <span>Пишите нам на почту:</span><br/>
                        a.ivlev@taho54.ru<br/>
                        e.semenov@taho54.ru  
                    </div>
                    <img style="margin-left: 36px;" src="/img/logo.png"/>
                </div>
            </div>
            <div class="main_footer" style="height:54px;  margin: 0;">
                <div class="container">
                    <div class="footer_text_left">
                        © 2015 TeamForse. All rights reserved.
                    </div>
                    <a href="http://webarchitector.net/" class="footer_text_right">
                        Сайт создан: WEBARCHITECTOR
                    </a>
                </div>
            </div>
        </div>
        
            
        <!--Начало формы отправки письма-->
        
            <?php
            $contactForm = new ContactForm;
        ?>
        <?php
            $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'formDialog',
                // additional javascript options for the dialog plugin
                'options'=>array(
                    'draggable'=>false,
                    'resizable'=>false,
                    'modal'=>true,
                    'title'=>FALSE,
                    'autoOpen'=>false,
                    'width'=>'380',
                    'height'=>'412',
                    'open'=>'js:function(){$(\'.ui-widget-overlay\').click(function(){
                        $(\'#contactDialog\').dialog(\'close\');
                    })}',
                ),
            )); 
        ?>
        <div class="super_form">
            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form2',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form2').attr('action'),
                                data: $('#contact-form2').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form2').html('<div class=\"form_title\"><br/>Благодарим вас за заявку<br/>Мы скоро свяжемся с Вами</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

                <div class="form_title">
                    Заполните форму<br/>
                    и наш менеджер скоро<br/>
                    с Вами свяжется
                </div>
                <?= $form->textFieldControlGroup($contactForm,'name', array(
                    'placeHolder'=>'Контактное лицо',
                )); ?>

                <?= $form->textFieldControlGroup($contactForm,'phone', array(
                    'placeHolder'=>'Телефон',
                )); ?>

                <?= $form->textFieldControlGroup($contactForm,'email', array(
                    'placeHolder'=>'E-mail',
                )); ?>

                <?=BsHtml::submitButton('ОТПРАВИТЬ', array(
                    'class' => 'float_right',
                ))?>

            <?php $this->endWidget(); ?>
            </div>
        <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
        
        <!--Конец формы отправки письма-->
        <script type="text/javascript">	
            $(function() {
            var Page = (function() {
                var $navArrows = $( '#nav-arrows' ),
                    $nav = $( '#nav-dots > span' ),
                    slitslider = $( '#slider' ).slitslider( {
                        onBeforeChange : function( slide, pos ) {
                            $nav.removeClass( 'nav-dot-current' );
                            $nav.eq( pos ).addClass( 'nav-dot-current' );
                        }
                    } ),
                    init = function() {
                        initEvents();
                    },
                    initEvents = function() {
                        // add navigation events
                        $navArrows.children( ':last' ).on( 'click', function() {
                            slitslider.next();
                            return false;
                        } );
                        $navArrows.children( ':first' ).on( 'click', function() {
                            slitslider.previous();
                            return false;
                        } );
                        $nav.each( function( i ) {
                            $( this ).on( 'click', function( event ) {
                                var $dot = $( this );
                                if( !slitslider.isActive() ) {
                                    $nav.removeClass( 'nav-dot-current' );
                                    $dot.addClass( 'nav-dot-current' );
                                }
                                slitslider.jump( i + 1 );
                                return false;
                            } );
                        } );
                    };
                    return { init : init };
            })();
            Page.init();
            /**
             * Notes: 
             * 
             * example how to add items:
             */
            /*
            var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');
            // call the plugin's add method
            ss.add($items);
            */
        });
        </script>
        <script type="text/javascript">
            /* init Call Service */
            var CallSiteId = '041f0820fd5f538dc2752e0789122b9f';
            var CallBaseUrl = '//uptocall.com';
            (function() {
                var lt = document.createElement('script');
                lt.type ='text/javascript';
                lt.charset = 'utf-8';
                lt.async = true;
                lt.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + CallBaseUrl + '/widget/client.js?rnd='+Math.floor(Math.random(0,1000)*1000);
                var sc = document.getElementsByTagName('script')[0];
                if (sc) sc.parentNode.insertBefore(lt, sc);
                else document.documentElement.firstChild.appendChild(lt);
            })();
        </script>
    </body>
</html>



