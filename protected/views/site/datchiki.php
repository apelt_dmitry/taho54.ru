
<div class="select_bgr_1">
    <div class="container ">
        <div class="mercury_title_line speedometers_title" style="margin-top: 40px;">
           В соответствии с п.3И приказа Министерства труда и социальной защиты Российской Федерации от 10.12.2012г. №580Н «Правилами финансового обеспечения предупредительных мер по сокращению производственного травматизма и профессиональных заболеваний работников и санаторно-курортного лечения работников, занятых на работах с вредными и (или) опасными производственными факторами»
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Mercury content START -->
    <div style="width: 806px; float: right; margin-top: 40px; position: relative;">
        <div class="catalog_box  ">
            <img src="/img/ebal_ya_datchiki.png"/>
        </div>
        <div class="cat_box_text_2">
            <div class="catalog_text_title_2"><img class="malevich_2" src="/img/malevich_2.png">Датчики скорости электронного тахографа</div>
            При установке современного тахографа, его необходимо подключить к датчику скорости для корректной регистрации данных. При этом следует учитывать, что с тахографом СКЗИ, Техрегламент и ЕСТР совместимы не все типы датчиков.
            Преимущественно, транспортные средства, используемые для коммерческих перевозок грузов в России довольно старые, ситуация в пассажироперевозках немногим отличается. На моделях грузовиков КамАЗ до 2007 г.в., ЛиАЗ, автобусах ПАЗ, микроавтобусах ГАЗ, ЗИЛ, КАЗ спидометр как правильно механический с тросиком, а датчик скорости аналоговый. Замена датчика скорости на цифровой – обязательна. 
            Иногда установить датчик скорости требуется и на иномарку с электронным спидометром, например, на некоторые модели Volkswagen, Ford, Mercedes. Стоимость замены датчика скорости складывается из цены самого датчика, стоимости проводов и услуг по установке. Своими руками датчик заменить нельзя, так как он пломбируется лицензированными организациями.
            В «ТИМФОРС» вы можете узнать, нужно ли вам менять датчик скорости и спидометр для установки тахографа на ваше транспортное средство. 
        </div>
        <div class="speed_row_top">
            В ассортименте:
        </div>
        <div class="speed_row_bot">
            Датчик PTF KITAS2+ L=25,0 mm 2171.20002310	<br/>
            Датчик PTF KITAS2+ L=63.2mm 2171.20002410	<br/>
            Датчик PTF KITAS2+ L=63.2mm 2171.20302410	<br/>
            Датчик PTF KITAS2+ L=63.2mm U=1,2MM 2171.200024510	<br/>
            Датчик PTF KITAS2+ М22*1.5R 2171/01000010	<br/>
            Датчик скорости (63,2 мм)	<br/>
            Датчик скорости 4202.3843-ТУ на тахограф электронный	<br/>
            Датчик скорости 4202.3843010	<br/>
            Датчик скорости KITAS L=63,2	<br/>
            Датчик скорости ДСА 345.3843	<br/>
            Датчик скорости ДСД 2112 (кореец) - 3.5мм	<br/>
            Датчик скорости импульсный 4202.3843010 (Бочонок)	<br/>
            Датчик скорости импульсный 4222.3843010 (Камаз)	<br/>
            Датчик скорости импульсный 4402 (35мм)	<br/>
            Датчик скорости импульсный 4412 (90мм)	<br/>
            Датчик скорости импульсный 4422 (25мм)	<br/>
            Датчик скорости импульсный ДСЭ-2 (КАМАЗ)	<br/>
            Датчик скорости импульсный ЗИЛ/ПАЗ	<br/>
            Датчик скорости импульсный ПД8093 (19,8 мм)	<br/>
            Датчик скорости импульсный ПД8093-3 (63,2 мм)	<br/>
            Датчик скорости импульсный ПД8136 (Бочонок)	<br/>
            Датчик скорости на корейца (проходной)	<br/>
            Датчик спидометра ПАЗ, КАМАЗ, МАЗ,ЛиАЗ (4222.3843)	<br/>
            Мидас 2171.01-М22Х1,5 внутренний	<br/>
            Мидас 2171.632 AMP	<br/>
            Переходник на "Американец"	<br/>
            Переходник на "Кореец"	<br/>
            Переходник на "Мерседес"	<br/>
            Привод спидометра КАМАЗ АВТОПРИБОР 4222.3843	<br/>
            Штуцер "ГАЗель"	<br/>
            Штуцер "ЗИЛ"	<br/>
            Штуцер "ПАЗ"	<br/>
        </div>
    </div>
    <!-- Mercury content END -->
</div>
<!-- Index FOOTER START -->