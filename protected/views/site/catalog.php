
<div class="select_bgr_1">
    <div class="container ">
        <div class="catalog_title">
           В соответствии с п.3И приказа Министерства труда и социальной защиты Российской Федерации от 10.12.2012г. №580Н «Правилами<br/> финансового обеспечения предупредительных мер по сокращению производственного травматизма и профессиональных заболеваний<br/> работников и санаторно-курортного лечения работников, занятых на работах с вредными и (или) опасными производственными факторами»
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Catalog START -->
    <div style=" width: 814px;
            float: right;
            margin-top: 28px;
            position: relative;">
        <div class="catalog_box">
            <div class="catalog_box_title">VDO DTCO 3283 <br/>с СКЗИ</div>
            <img class="cat_1" src="/img/3283.png"/>
            <a href="/site/DTCO3283"class="cat_but_1">Подробнее</a>
        </div>
        <div class="catalog_box ml23">
            <div class="catalog_box_title">Международный <br/>тахограф VDO 1381</div>
            <img class="cat_1" src="/img/1381.png"/>
            <a href="continental1381" class="cat_but_1">Подробнее</a>
        </div>
        <div class="catalog_text_box ml18">
            <div class="catalog_text_title"><img class="malevich_1" src="/img/malevich_1.png"/>ТАХОГРАФЫ</div>
            <div class="catalog_text_main">
                Наша компания имеет все полномочия для монтажа, ремонта, диагностики и полного обслуживания тахографов. 
                Это подкреплено официальным разрешением Министерства транспорта РФ, и имеет соответствующий регистрационный номер. 
                <p style="margin-top: 7px;">Все специалисты прошли необходимое обучение в сертифицированном учебном центре.</p>
            </div>
        </div>
        <div class="catalog_box mt32">
            <div class="catalog_box_title">Atol Drive 5 с<br/>блоком СКЗИ</div>
            <img class="cat_1" src="/img/atol.png"/>
            <a href="/site/atol" class="cat_but_1">Подробнее</a>
        </div>
        <div class="catalog_box ml23 mt32">
            <div class="catalog_box_title">VDO Continental<br/>с СКЗИ и ГЛОНАСС</div>
            <img class="cat_2" src="/img/continental.png"/>
            <a href="/site/continental" class="cat_but_1">Подробнее</a>
        </div>
        <div class="catalog_box ml23 mt32">
            <div class="catalog_box_title">Меркурий ТА-001<br/>с блоком СКЗИ</div>
            <img class="cat_1" src="/img/mercury_catalog.png"/>
            <a href="/site/mercury" class="cat_but_1">Подробнее</a>
        </div>
        <div class="ll2"></div>
        <div class="catalog_box  ">
            <div class="catalog_box_title pt20">СПИДОМЕТРЫ</div>
            <img class="cat_3" src="/img/speedometers.png"/>
            <a href="/site/speedometers" class="cat_but_1">Подробнее</a>
        </div>
        <div class="cat_box_text_2">
            <div class="catalog_text_title_2"><img class="malevich_2" src="/img/malevich_2.png"/>СПИДОМЕТРЫ ДЛЯ ЦИФРОВОГО ТАХОГРАФА СКЗИ, ЕСТР И ТР</div>
            Чтобы цифровой тахограф работал, при установке его подключают к импульсному датчику скорости, благодаря этому тахограф рассчитывает скорость и пройденное расстояние и отправляет эти данные на электронный спидометр. Данное взаимодействие обеспечивает корректную работу тахографа. Допустимые к использованию для мониторинга транспорта тахографы СКЗИ, ЕСТР и Техрегламент не совместимы с механическими (аналоговыми) спидометрами и датчиками скорости, поэтому их приходится заменять. В основном механический спидометр с тросиком используется на российских автомобилях старше 2007 г.в.: грузовиках КамАЗ, ЛиАЗ, автобусах ПАЗ, микроавтобусах ГАЗ, ЗИЛ, КАЗ
        </div>
        <div class="ll2"></div>
        <div class="catalog_box  ">
            <div class="catalog_box_title pt20">ДАТЧИКИ СКОРОСТИ</div>
            <img class="cat_3" src="/img/datchik.png"/>
            <a href="/site/datchiki" class="cat_but_1">Подробнее</a>
        </div>
        <div class="cat_box_text_2">
            <div class="catalog_text_title_2"><img class="malevich_2" src="/img/malevich_2.png"/>ДАТЧИКИ СКОРОСТИ ЭЛЕКТРОННОГО ТАХОГРАФА</div>
            При установке современного тахографа, его необходимо подключить к датчику скорости для корректной регистрации данных. При этом следует учитывать, что с тахографом СКЗИ, Техрегламент и ЕСТР совместимы не все типы датчиков.
            <p>Преимущественно, транспортные средства, используемые для коммерческих перевозок грузов в России довольно старые, ситуация в пассажироперевозках немногим отличается. На моделях грузовиков КамАЗ до 2007 г.в., ЛиАЗ, автобусах ПАЗ, микроавтобусах ГАЗ, ЗИЛ, КАЗ спидометр как правильно механический с тросиком, а датчик скорости аналоговый. Замена датчика скорости на цифровой – обязательна. </p>
        </div>
    </div>
    <!-- Catalog END -->
</div>
<!-- Index FOOTER START -->