
<div class="select_bgr_1">
    <div class="container ">
        <div class="mercury_title_line" style="margin-top: 64px;">
           МЕЖДУНАРОДНЫЙ ТАХОГРАФ VDO CONTINENTAL (КОНТИНЕНТАЛ) 1381
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Mercury content START -->
    <div style="width: 795px; float: right; margin-top: 40px; position: relative;">
        <div>
            <div style="float: left">
                <img src="/img/best_buy_red.png" class="contintal_best_buy"/>
                <img class="cont_img" style="z-index: 2;"src="/img/vdo_continental.png"/>
                <a href="#" class="continental_button" onclick="$('#formDialog').dialog('open'); return false;"></a>
                <div class="under_cont">
                    Соответсвует регламенту ЕСРТ,<br/> 
                    что позволяет осуществлять<br/>
                    международные автомобильные<br/>
                    перевозки
                </div>
            </div>
            <div class="right_cont">
                Немецкий цифровой ЕСТР-тахограф, который рекомендует большинство европейских автопроизводителей за надежность, простоту и бесперебойность. Технологичность и удобный интерфейс тахографа – причина востребованности данного ЕСТР-тахографа среди российских дальнобойщиков. Поскольку выезд в международные рейсы с тахографом СКЗИ запрещен, 
                Siemens VDO 1381 – лучший для осуществления грузо- и пассажироперевозок за границу..
                Калибровку могут осуществлять исключительно авторизованные центры. Калибровку тахографа ЕСТР в Новосибирске вы можете заказать в мастерской «ТИМФОРС»
            </div>
            
        </div>
        
        <div class="merc_box_2">
            <div class="merc_text_blue">
               Почему тахограф VDO (ВДО) 1381?
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png" />
                <img style="margin-top: 32px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 5px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 32px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 6px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 5px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Соответствует требованиям ЕСТР, в отличие от других тахографов может использоваться на<br/>
                коммерческих рейсах в страны Европы<br/>
                Прост в управлении. Любой водитель легко освоит работу с тахографом<br/>
                Рекомендован к установке на европейских автомобилях, таких как Mercedes-Benz, MAN, Scania,<br/>
                Ford, Volvo, DAF, Renault, Furso<br/>
                Надежное устройство, показывает стабильную работу, независимо от уровня вибрации в кабине<br/>
                Возможность задать предел скорости, звуковой сигнал, при превышении предела<br/>
                Функция "помощник водителя" <br/>
                Слот для интеллектуального датчика движения KITAS 2+<br/>
                22 языка пользователя доступны в DTCO<br/>
                Сигнал превышения заранее выбранного предела скорости; <br/>
                Активируемый сигнал после 4-15 ч вождения
            </div>
        </div>
        <div class="merc_box_2">
            <div class="merc_text_blue">
               Функциональные особенности тахографа VDO DTCO 1381
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png" />
                <img style="margin-top: 2px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
            Обеспечивает выполнение требований Европейского соглашения к международным перевозкам<br/>
            Позволяет российским транспортным компаниям работать за рубежом<br/>
            Не имеет блока СКЗИ<br/>
            Разрешен к установке на территории России<br/>
            Устанавливается на иномарки и российские автомобили любых моделей<br/>
            Поддерживает карты тахографа ЕСТР (Европейского стандарта)
            </div>
        </div>
        <div style="float: left; margin-top: 30px;">
            <table  class="table_merc" >
                <tr>
                    <td class="td_merc_title" colspan="10" >
                        Технические характеристики VDO Continental (Континентал) 1381  
                    </td>
                </tr>
                <tr>
                    <td>Соответствие Европейскому стандарту ЕСТР</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Разрешен к установке на ТС, предназначенные для перевозки опасных грузов</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Блок криптографической защиты (СКЗИ)</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Беспроводные технологии</td>
                    <td>DLD Short Range и <br/>DLD Wide Range</td>
                </tr>
                <tr>
                    <td>USB-разъем</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Диапазон рабочих температур тахографа, °С</td>
                    <td>-20…+70</td>
                </tr>
                <tr>
                    <td>Пределы регистрации скорости, км/час</td>
                    <td>до 220</td>
                </tr>
                <tr>
                    <td>Напряжение питания, В</td>
                    <td>12…24 </td>
                </tr>
                <tr>
                    <td>Скоростной принтер</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>K-Line диагностика </td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Габаритные размеры</td>
                    <td>178х50х150 </td>
                </tr>
                <tr>
                    <td>Масса, кг, не более</td>
                    <td>1.0</td>
                </tr>
            </table>
        </div>
        
    </div>
    <!-- Mercury content END -->
</div>
<!-- Index FOOTER START -->