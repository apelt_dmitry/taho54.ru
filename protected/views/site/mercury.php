
<div class="select_bgr_1">
    <div class="container ">
        <div class="mercury_title_line" style="margin-top: 64px;">
           Тахограф Меркурий ТА-001 с блоком СКЗИ
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Mercury content START -->
    <div style="width: 795px; float: right; margin-top: 40px; position: relative;">
        <img class="mercury_img"src="/img/mercury.png"/>
        <a href="#" class="mercury_button" onclick="$('#formDialog').dialog('open'); return false;"></a>
        <div class="merc_box_1">
            <div class="merc_text_blue">
                Функциональные особенности цифрового<br/> тахографа Меркурий:
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png" />
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png" />
                <img style="margin-top: 5px;"src="/img/forfeit_galka.png" />
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png" />
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Прост в установке, активации и калибровке<br/>
                Имеет USB, Bluetooth, GPRS интерфейсы<br/>
                Может использоваться 2-мя водителями<br/>
                Подходит для любых типов КПП и<br/>
                устанавливается на все категории транспорта<br/>
                М2, М3, N2, N3
            </div>
        </div>
        <div class="merc_box_2">
            <div class="merc_text_blue">
                Почему выбирают тахограф Меркурий ТА-001
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png" />
                <img style="margin-top: 5px;" src="/img/forfeit_galka.png" />
                <img style="margin-top: 31px;"src="/img/forfeit_galka.png" />
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Сравнительно низкая стоимость<br/>
                Наличие голосовой связи водителя с диспетчером, тревожная кнопка, возможность отправлять <br/>текстовые сообщения<br/>
                Тахограф прост в эксплуатации
            </div>
        </div>
        <div style="float: left;">
            <table  class="table_merc" >
                <tr>
                    <td class="td_merc_title" colspan="10" >
                        Технические характеристики ВДО Континенталь 3283   
                    </td>
                </tr>
                <tr>
                    <td>Соответствие «Приказ Минтранс №36» (встроен блок СКЗИ, внесен в реестр тахографов ФБУ Росавтотранс)</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Разрешен к установке на ТС, предназначенные для перевозки опасных грузов</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Блок криптографической защиты (СКЗИ)</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Сбор статистики и хранение информации, дней</td>
                    <td>365</td>
                </tr>
                <tr>
                    <td>Частотные диапазоны GPRS модема, МГц</td>
                    <td>900, 1800 </td>
                </tr>
                <tr>
                    <td>Размеры термохимической бумаги: - ширина, мм</td>
                    <td>57,5</td>
                </tr>
                <tr>
                    <td>USB-разъем</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Диапазон рабочих температур тахографа, °С</td>
                    <td>-40…+70</td>
                </tr>
                <tr>
                    <td>Пределы регистрации скорости, км/час</td>
                    <td>до 220</td>
                </tr>
                <tr>
                    <td>Напряжение питания, В</td>
                    <td>1.2 </td>
                </tr>
                <tr>
                    <td>Габаритные размеры</td>
                    <td>210x190x65 </td>
                </tr>
                <tr>
                    <td>Масса, кг, не более</td>
                    <td>1.2 </td>
                </tr>
            </table>
        </div>
        
    </div>
    <!-- Mercury content END -->
</div>
<!-- Index FOOTER START -->