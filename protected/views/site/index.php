<div class="main_slider_top">
    <div id="slider" class="sl-slider-wrapper ">
        <div class="sl-slider">
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-1"></div>
                    <div class="container">
                        <div class=" baba"></div>
                        <div class="slider_1_text_1 ">   
                            <span>ОФОРМЛЯТЬ КАРТУ ТАХОГРАФА ХЛОПОТНО И ДОЛГО?</span><br/>
                            Забудьте! Он-лайн оформление карты водителя и карты <br/>
                            предприятия за 5 минут! Доставляем по всей Росии
                        </div>
                        <div class="slider_1_text_2">
                            <div class="galochka"></div>Никакой бюрократии
                        </div>
                        <div class="slider_1_text_2">
                            <div class="galochka"></div>Оформление просто как 1-2-3.
                        </div>
                        <div class="slider_1_text_2">
                            <div class="galochka"></div>Вы приезжаете уже за готовой картой
                        </div>
                        <div class="slider_1_text_2">
                            <div class="galochka"></div>Экономия более 4 часов вашего времени 
                        </div>
                        <div class="slider_1_text_2">
                            <div class="galochka"></div>Оформляй карту из любой точки России
                        </div>
                        <a href="/site/map_selection" class="slider_1_button_1">
                            Оформите карту он-лайн
                        </a>
                    </div>
                </div>
            </div>
            <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-2"></div>
                    <div class="container">
                        <div class="mujik"></div>
                        <div class="slider_1_text_1 slider_1_text_3">   
                            Мобильная бригада мастеров бесплатно<br/>
                            приедет к вам для установки тахографа
                        </div>
                        <div class="slider_1_text_4">
                            Мы осуществляем установку тахографов с<br/>
                            выездом к клиенту, по Новосибирску<br/> 
                            выезд бесплатный. Калибровку и<br/> 
                            сервисное обслуживание вы также<br/> 
                            можете «заказать с доставкой»<br/> 
                            в любую точку города!
                        </div>
                        <a href="#" class="slider_1_button_2" onclick="$('#formDialog').dialog('open'); return false;">
                            Закажите бесплатный выезд
                        </a>
                    </div> 
                </div>
            </div>
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                <div class="sl-slide-inner">
                    <div class="bg-img bg-img-3"></div>
                    <div class="container">
                        <div class="taho"></div>
                    </div> 
                </div>
            </div>
        </div><!-- /sl-slider -->
        <nav id="nav-arrows" class="nav-arrows">
                <span class="nav-arrow-prev">Previous</span>
                <span class="nav-arrow-next">Next</span>
        </nav>
    </div><!-- /slider-wrapper -->
</div>       

        <!-- Наши клиенты начало-->
        <div class="slider_2">
            <div class="container">
                <div class="klienti">Наши клиенты</div>
                <div class="single-item">
                    <div class="klient1"></div>
                    <div class="klient2"></div>
                    <div class="klient3"></div>
                </div>
            </div>
        </div>
        <div style="width: 100%; height: 2px; background-color: #d3d3d3;"></div><!-- Разделительная полоса -->
        <!-- Наши клиенты конец-->
        <!-- Index (содержание главной) Start -->
        <div class="container">
            <!-- Left Widget Start -->
            <div style="width: 284px;float: left;margin-top: 30px;">
                <!-- Callback widget Start -->
                <div class="call_me">
                    <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
                </div>
                <!-- Callback widget End -->
                <!-- Form widget Start -->
                <div class="spec_form">
                    <!--Начало формы отправки письма-->
                    <?php $contactForm = new ContactForm; ?>

                    <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                        'id'=>'contact-form',
                        'action'=>array('site/contact'),
                        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                        'enableAjaxValidation'=>true,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnChange'=>false,
                            'validateOnSubmit'=>true,
                            'afterValidate' => "js: function(form, data, hasError) {
                                if ( !hasError) {
                                    $.ajax({
                                        type: 'POST',
                                        url: $('#contact-form').attr('action'),
                                        data: $('#contact-form').serialize(),
                                        success: function(data_inner) {
                                            if ( data_inner==1 ) {
                                                $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                            } else {
                                                alert('Хъюстон у нас проблемы!!!!');
                                            }
                                        }
                                    });
                                }
                                return false;
                            }
                            ",
                        ),
                    )); ?>

                    <?= $form->textFieldControlGroup($contactForm,'name', array(
                        'placeHolder'=>'Контактное лицо',
                    )); ?>

                    <?= $form->textFieldControlGroup($contactForm,'phone', array(
                        'placeHolder'=>'Телефон',
                    )); ?>

                    <?=BsHtml::submitButton('', array(
                        'class' => 'float_right',
                    ))?>

                    <?php $this->endWidget(); ?>
                    <!--Конец формы отправки письма-->
                </div>
                <!-- Form widget End -->
                <!-- Otziv slider Start -->
                <div class="slider_3">
                    <div style="width:100%;">
                        <div class="otzivi">
                            <div class="otziv">
                                <div class="name_otziv">
                                    Александр Миллер
                                </div>
                                <div class="dolzjnost_otziv">
                                    “Газпром” Директор
                                </div>
                                <div class="text_otziv">
                                    Lorem Ipsum is simply dummy
                                    text of the printing and
                                    typesetting industry. Lorem 
                                    Ipsum has been the industry's
                                    standard dummy text ever since
                                    the 1500s, when an unknown 
                                    printer took a galley of type
                                    and scrambled it to make a type
                                    specimen book. It has survived
                                    not only five 
                                    centuries, but also the leap into
                                    electronic typesetting, remaining
                                </div>
                            </div>
                            <div class="otziv">
                                <div class="name_otziv">
                                    Александр Миллер
                                </div>
                                <div class="dolzjnost_otziv">
                                    “Газпром” Директор
                                </div>
                                <div class="text_otziv">
                                    Lorem Ipsum is simply dummy
                                    text of the printing and
                                    typesetting industry. Lorem 
                                    Ipsum has been the industry's
                                    standard dummy text ever since
                                    the 1500s, when an unknown 
                                    printer took a galley of type
                                    and scrambled it to make a type
                                    specimen book. It has survived
                                    not only five 
                                    centuries, but also the leap into
                                    electronic typesetting, remaining
                                </div>
                            </div>
                            <div class="otziv">
                                <div class="name_otziv">
                                    Александр Миллер
                                </div>
                                <div class="dolzjnost_otziv">
                                    “Газпром” Директор
                                </div>
                                <div class="text_otziv">
                                    Lorem Ipsum is simply dummy
                                    text of the printing and
                                    typesetting industry. Lorem 
                                    Ipsum has been the industry's
                                    standard dummy text ever since
                                    the 1500s, when an unknown 
                                    printer took a galley of type
                                    and scrambled it to make a type
                                    specimen book. It has survived
                                    not only five 
                                    centuries, but also the leap into
                                    electronic typesetting, remaining
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Otziv slider End -->
                <!-- za4em??? Start -->
                <div class="za4em">
                    <div class="za4em_text2">
                        Зачем нужен тахограф? 
                    </div>
                    <div class="za4em_text">
                        <div class="galochka"></div>
                        Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
                    </div>
                    <div class="za4em_text">
                        <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
                    </div>
                    <div class="za4em_text">
                        <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
                    </div>
                    <div class="za4em_text">
                        <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
                    </div>
                </div>
                <!-- za4em??? END -->
            </div>
            <!-- Left Widget End -->
            <!-- Index content START -->
            <script>
                $(function() {
                    $( "#tabs" ).tabs({
                      event: "mouseover"
                    });
                });
            </script>
            <div id="tabs">
                <ul>
                    <li class="cr_1"><a href="#tabs-1">Тахографы</a></li>
                    <li class="cr_2"><a href="#tabs-2">Оформить карту тахографа onlain</a></li>
                </ul>
                <div id="tabs-1">
                    <div class="crazy_bgr"></div>
                    <div class="crazy_text">
                        <div class=" crazy_h1">
                            Цифровой тахограф <br/>
                            «VDO DTCO 3283» с СКЗИ 
                        </div>
                        <div class="crazy_h2">
                            Подходит для установки на<br/>
                            транспортные средства любой<br/>
                            марки, любого года выпуска.
                        </div>
                        <div class="crazy_h3">
                            - Немецкий тахограф,<br/> рекомендованный ведущими<br/> автопроизводителями
                            <div style="margin-top: 5px;">
                            -  удаленное снятие отчетов 
                            </div>
                            <div style="margin-top: 5px;">
                            -  отображение текстовых<br/> сообщений водителю и голосовая<br/> связь
                            </div>
                        </div>
                        <a href="/site/DTCO3283" class="crazy_button" style="left:60px">Подробнее</a>
                    </div>
                    <div class="crazy_text">
                        <div class=" crazy_h1">
                            Тахограф АТОЛ Drive 5<br/>
                            с блоком криптозащиты 
                        </div>
                        <div class="crazy_h2">
                            Самый надежный тахограф
                        </div>
                        <div class="crazy_h3">
                            - подходит для российских ТС и<br/>
                            иномарок категорий N2, N3, M2,<br/>
                            M3, в т.ч. перевозящих опасные<br/>
                            грузы
                            <div style="margin-top: 5px;">
                                - слот для подключения<br/>
                                доп.модулей контроля за<br/>
                                транспортом<br/>
                            </div>
                            <div style="margin-top: 5px;">
                                - блок СКЗИ
                            </div>
                            <div style="margin-top: 5px;">
                                - эксклюзив! Тахограф на<br/>
                                подмену при ремонте<br/>
                            </div>
                        </div>
                        <a href="/site/atol"class="crazy_button"style="left:336px">Подробнее</a>
                    </div>
                    <div class="crazy_text">
                        <div class=" crazy_h1">
                            Тахограф ЕСТР <br/>
                            «VDO Continental 1381» 
                        </div>
                        <div class="crazy_h2">
                            Позволяет осуществлять<br/>
                            внутрироссийские и <br/>
                            заграничные коммерческие<br/>
                            поездки
                        </div>
                        <div class="crazy_h3">
                             - предустановлен на <br/>большинстве европейских<br/> автомобилей
                             <div style="margin-top: 5px;">
                                 -  отсутствие блока СКЗИ<br/> позволяет выезжать в<br/> коммерческие рейсы за границу
                             </div>
                        </div>
                        <a href="/site/continental1381" class="crazy_button"style="left:612px">Подробнее</a>
                    </div>
                </div>
                <div id="tabs-2">
                    <div style="margin: 27px 21px 0 16px;">
                        <img class="cr_ha"src="/img/cr_ha.png"/>
                        <p class="cr_ha_t_1">Карта тахографа – специализированный ключ, предназначенный для персонализации, без которого невозможна работа тахографа. Компания Team Force реализует 2 категории карт тахографа: водительские карты и карты предприятия.</p>
                        <p class="cr_ha_t_2">Карта водителя выдается персонально в одном экземпляре и идентифицирует водителя транспортного средства. В процессе вождения непрерывно находится в тахографе и позволяет фиксировать сведения о скорости 
                            движения, дислокации и режиме труда и отдыха за определенным водителем.
                            <br/>Карта предприятия – корпоративная карта компании, которая выдается на имя руководителя или 
                            иного должностного лица в одном или нескольких экземплярах. Информация на карте не 
                            записывается и не хранится – её назначение – получение доступа к данным всех цифровых 
                            тахографов, установленных на машины автопарка предприятия. Срок действия карты – 5 лет.  </p>
                        <p class="cr_ha_t_1">Теперь оформить карту водителя ЕСТР, карточку водителя СКЗИ и техрегламент, а также карту предприятия вы можете в режиме он-лайн на нашем сайте.</p>
                        <a href="site/map_selection" class="cr_btn"></a>
                    </div>
                  </div>
            </div>
            
            <div class="min_5">
                <a href="/site/map_selection" class="min_5_button sprite1"></a>
            </div>
            <div class="text1" style="  float: left;
                                        text-align: left;
                                        width: 832px;
                                        margin-left: 24px;
                                        margin-top: 30px;">
                <span style="line-height: 30px;
                      font-size: 24px;
                      font-family: MyriadProBold">Наши услуги</span><br/>
                Наша комппания, при выполении работ руководствуется принципом
                того,что свою работу нужно делать качественно и в срок. Мы ценим
                не только Ваше время, но и свое. Поэтому, можете быть уверенны в
                том, что все необходимые процедуры связанные с активацией,
                калибровкой, настройкой тахографа будут сделаны согласно всех
                предписывающих инструкций и требований
            </div>
            <div class="main_ta">
                <a  href="/site/services#nik1" class="main_ta_button_1">Установка</a>
                <a  href="/site/services#nik2" class="main_ta_button_2">Настройка</a>
                <a  href="/site/services#nik3" class="main_ta_button_3">Калибровка</a>
                <a  href="/site/services#nik4" class="main_ta_button_4">Изготовление карт водителя</a>
                <a  href="/site/services#nik5" class="main_ta_button_5">Техническое обслуживание</a>
            </div>
            <div style="position: relative;
                 width: 100%;
                 height: 350px;
                 float: right;
                 margin-top: 60px;">
                <div class="green_car"></div>
                <div class="text1" style="position: absolute;
                     width: 450px;
                     height: 260px;
                     right: 0;
                     top:0; ">
                    <span style="line-height: normal;
                          font-family:MyriadProBold;
                          text-transform: uppercase;
                          font-size: 24px;">Мобильная мастерская по<br/> установке тахографов<br/>
                    </span>
                    При установке тахографа предприятия и водители сталкиваются с неудобствами:
                    транспорт снимают с рейса, график сбивается, водитель 2 часа тратит только 
                    на дорогу до мастерской и обратно. Чтобы сэкономить ваше время и деньги, мы
                    осуществляем установку тахографов с выездом к клиенту, по Новосибирску выезд
                    бесплатный. Калибровку и сервисное обслуживание вы также можете «заказать с
                    доставкой» в любую точку города!
                </div>
                <a href="#" class="green_car_button" onclick="$('#formDialog').dialog('open'); return false;">Закажите бесплатный выезд </a>
            </div>
            <div class="ll"></div>
            <div style="position: relative;
                 width: 100%;
                 height: 430px;
                 float: right;
                 text-align: center;">
                <div class="text1" style="margin: 30px 0 30px 0;">
                    <span style="line-height: normal;
                          font-family:MyriadProBold;
                          text-transform: uppercase;
                          font-size: 24px;">
                        Тахограф на подмену</span>
                </div>
                <div class="image1"></div>
                <div class="text1" style="text-align: center; margin: 30px 0 30px 0;">
                    Случаи поломок тахографов достаточно редки. Самые безотказные тахографы смотрите здесь. Но если все же прибор выйдет из строя,<br/>
                    неизбежно возникает проблема: коммерческие перевозки с неисправным тахографом (или без него) запрещены, и машина должна будет<br/>
                    простаивать до момента установки отремонтированного устройства. Ремонт, как правило, длится 7-30 дней. Какие расходы понесет за это<br/>
                    время ваша компания? С Team Force вы не потеряете ни рубля на простое! Мы являемся авторизованным центром продаж тахографов «АТОЛ»<br/> 
                    и предоставляем на них гарантию 2 года и тахограф на подмену в случае неисправности прибора. Установка дублера в день обращения.<br/>
                </div>
                <a  href="#" onclick="$('#formDialog').dialog('open'); return false;" class="price_button" style="margin-bottom: 30px;">
                    Расчитать стоимость
                </a>
            </div>
            <div class="ll"></div>
            <div style="position: relative;
                 width: 100%;
                 height: 330px;
                 float: right;">
                <div class="image2"></div>
                <div class="text1" style="position: absolute;
                     width: 830px;
                     height: 190px;
                     right: 0;
                     top:30px; 
                     text-align: left;">
                    <span style="line-height: normal;
                          font-family:MyriadProBold;
                          text-transform: uppercase;
                          font-size: 24px;">Снижаем издержки и риски<br/>
                    </span>
                    Считаете, что тахограф бесполезен и только осложняет работу водителя? Мы научим вас использовать<br/>
                    потенциал устройства по максимуму. Консультацию по преимуществам и недостаткам цифровых<br/>
                    тахографов разных марок вы можете получить в офисе Team Force. Чтобы выбрать хороший тахограф,<br/>
                    помимо цены нужно учитывать стабильность прибора в работе, стоимость обслуживания, устойчивость<br/>
                    к поломкам, дополнительные функции. Более 7-ми лет опыта на рынке систем мониторинга<br/>
                    автотранспорта позволяют предлагать вам оптимальные решения и гарантировать, что работы по<br/>
                    установке, активации, калибровке и обслуживанию будут проводиться качественно и в срок!<br/>
                </div>
                <a href="#" class="button_tf" onclick="$('#formDialog').dialog('open'); return false;">узнать о тахографах всё</a>
            </div>
            <!-- Index content END -->
        </div>
        <!-- Index FOOTER START -->
        <div class="footer_bgr">
            <div class="container">
                <div style="position: relative;
                     width: 100%;
                     height: 330px;
                     float: right;">
                    <div class="footer_baba"></div>
                    <div class="text1" style="position: absolute;
                         color: #ffffff;
                         width: 830px;
                         height: 190px;
                         right: 0;
                         top:76px; 
                         text-align: left;">
                        <span style="line-height: normal;
                              font-family:MyriadProBold;
                              text-transform: uppercase;
                              font-size: 24px;">Корпоративные решения<br/>
                        </span>
                        Компания Team Force ведет крупнейшие проекты в Новосибирске по оснащению системами<br/>
                        мониторинга и тахографами ведущих региональных предприятий и федеральных корпораций, таких<br/>
                        как РЖД, РусГидро, сеть гипермаркетов “Лента”. Мы привыкли предоставлять клиенту комплексное<br/>
                        решение, выдерживать договорные сроки. С 2015 года мы ввели систему он-лайн оформления карт<br/>
                        тахографа для водителей и предприятий, что сэкономит время на подготовку документов сотрудникам<br/>
                        компании. Также для наших партнеров предусмотрена система скидок и официальное партнерское<br/>
                        вознаграждение. Все это делает компанию Team Force лучшей мастерской по установке и сервису.<br/>
                    </div>
                    <a  href="#" class="button_baba_footer" onclick="$('#formDialog').dialog('open'); return false;">назначить персонального менеджера</a>
                </div>
            </div>
        </div>
        
        <!-- Index FOOTER END -->
        <!-- Index (содержание главной) End -->