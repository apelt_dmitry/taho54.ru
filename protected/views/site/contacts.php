<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerScriptFile('//api-maps.yandex.ru/2.1/?lang=ru_RU')
    ->registerScriptFile($pt.'js/icon_customImage.js');
?>
<script>
    $(document).ready(function(){
       $('.main_footer').css('margin-top','0px');
    });
</script>
<div style="width: 100%; height: 600px; position: relative;">

        <div id="map">
            <div class="container">
                <div class="contacts_bgr">
                    <div class="contacts_text_1">
                        Наш адрес:
                    </div>
                    <div class="contacts_text_2">
                        ул. Выборная 199/2, <br/>
                        компания "ТИМФОРС"
                    </div>
                    <div class="contacts_text_1">
                        Телефоны:
                    </div>
                    <div class="contacts_text_2">
                       8 (383) 3-109-110, <br/>
                       8 (383) 331-15-50
                    </div>
                    <div class="contacts_text_1">
                        Пишите нам на почту:
                    </div>
                    <div class="contacts_text_2">
                        <?= BsHtml::link('a.ivlev@taho54.ru', 'mailto:a.ivlev@taho54.ru', array('target'=>'_blank')) ?>,<br/>
                        <?= BsHtml::link('e.semenov@taho54.ru', 'mailto:e.semenov@taho54.ru', array('target'=>'_blank')) ?>
                    </div>
                </div>
            </div>
                
        </div>
        

</div>
