
<div class="select_bgr_1">
    <div class="container ">
        <div class="mercury_title_line speedometers_title" style="margin-top: 40px;">
           В соответствии с п.3И приказа Министерства труда и социальной защиты Российской Федерации от 10.12.2012г. №580Н «Правилами финансового обеспечения предупредительных мер по сокращению производственного травматизма и профессиональных заболеваний работников и санаторно-курортного лечения работников, занятых на работах с вредными и (или) опасными производственными факторами»
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Mercury content START -->
    <div style="width: 806px; float: right; margin-top: 40px; position: relative;">
        <div class="catalog_box  ">
            <img src="/img/ebal_ya_speedometri.png"/>
        </div>
        <div class="cat_box_text_2">
            <div class="catalog_text_title_2"><img class="malevich_2" src="/img/malevich_2.png">СПИДОМЕТРЫ ДЛЯ ЦИФРОВОГО ТАХОГРАФА СКЗИ, ЕСТР И ТР</div>
            Чтобы цифровой тахограф работал, при установке его подключают к импульсному датчику скорости, благодаря этому тахограф рассчитывает скорость и пройденное расстояние и отправляет эти данные на электронный спидометр. Данное взаимодействие обеспечивает корректную работу тахографа. Допустимые к использованию для мониторинга транспорта тахографы СКЗИ, ЕСТР и Техрегламент не совместимы с механическими (аналоговыми) спидометрами и датчиками скорости, поэтому их приходится заменять. В основном механический спидометр с тросиком используется на российских автомобилях старше 2007 г.в.: грузовиках КамАЗ, ЛиАЗ, автобусах ПАЗ, микроавтобусах ГАЗ, ЗИЛ, КАЗ
            <span style="font-weight: 900; margin: 10px 0; display: block;">- Нужно ли менять спидометр при установке тахографа? </span>
            Да нужно, если спидометр механический. Самому заменить спидометр на транспортном средстве не получится, поскольку он должен быть обязательно опломбирован и откалиброван. Различаются спидометры размерами, стоимостью, пределами измерения скорости, совместимостью с марками автомобилей.
            В тахографической мастерской Teem Force вы получите полную консультацию по необходимости замены спидометра и моделям, которые оптимально подойдут для вашего автомобиля.  
        </div>
        <div class="speed_row_top">
            В ассортименте:
        </div>
        <div class="speed_row_bot">
            CAN-спидометр 874.3802 "Автоприбор"<br/>
            Рамка переходная для спидометра 100-140мм (комплект)<br/>
            Рамка переходная для спидометра 100/140мм, квадратная<br/>
            Спидометр КАМАЗ, МАЗ, ПАЗ, ЗИЛ 811.3802<br/>
            Спидометр КАМАЗ, МАЗ, Урал, Краз, ПАЗ, ГАЗ ( 24В, 100мм)-87,3802<br/>
            Спидометр электронный 81.3802010 (140мм 24В)<br/>
            Спидометр электронный 853.3802010 (100 мм 12В)<br/>
            Спидометр электронный ПА 8046-4П (100мм 24В)<br/>
            Спидометр электронный ПА 8090<br/>
            Спидометр электронный ПА 8090 (140мм 24В)<br/>
            Спидометр электронный ПА 8090-2<br/>
            Спидометр электронный ПА 8160-4 (100мм 12В)<br/>
            Спидометр электронный ПА8168 CAN (100мм 12/24В)<br/>
        </div>
    </div>
    <!-- Mercury content END -->
</div>
<!-- Index FOOTER START -->