<script>
    $(document).ready(function(){
       $('.main_footer').css('margin-top','0px');
    });
</script>
<div class="select_bgr_1">
    <div class="container ">
       <div class="forfeit_header_line"></div>
       <div class="forfeit_text_1">
            Тахографы и российское законодательство.<br/>
            Как не попасть под административную ответственность
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Index content START -->
    <div class="services_div">
        <div class="forfeit_you"></div>
        <div class="forfeit_text_2">
            Тахогараф – устройства мониторинга автотранспорта, осуществляющее<br/>
            контроль соблюдения режима труда и отдыха водителя, а также<br/>
            скоростного режима автотранспорта. В России допустимо использование<br/>
            только цифровых тахографов трех разновидностей – устройства по<br/>
            международным стандартам ЕСТР, тахографы, соответствующие<br/>
            требованиям техрегламента и оборудование, оснащенное блоком<br/>
            криптографической защиты информации.
        </div>
        <a href="map_selection" class="forfeit_button">
            Выберите тахограф
        </a>
        <div class="suka"></div>
        <div class="news_line"></div>
    </div>
    
    <div class="services_div">
        <div class="forfeit_warning"></div>
        <div class="forfeit_text_3">
            <p>
                <em><strong>С 1.04.2014</strong></em> на водителя (или организацию), занимающуюся транспортировкой опасных грузов без установленного тахографа с СКЗИ шифровальным блоком, налагается штраф.
            </p>
            <p>
                <em><strong>С 1.06.2014</strong></em> введены санкции для грузовых ТС с массой свыше 15т, на которых не установлен прибор.
            </p>
            <p>
                <em><strong>С 1 сентября 2014 г.</strong></em> штрафуют при отсутствии тахографа с криптозащитой информации водителей грузовиков с разрешенной массой более 12т, не предназначенных для транспортировки опасных грузов.
            </p>
            <p>
                <em><strong>1 апреля 2015 года</strong></em> ответственность наступила и для транспортных средств категории N2 (с разрешенной массой 3,5 – 12 тонн).
            </p>
            
        </div>
        <div class="news_line"></div>
    </div>
    
    <div class="services_div">
        <div class="forfeit_text_4">
            За что штрафуют:
        </div>
        <div class="forfeit_police"></div>
        <div class="forfeit_text_5">
            В соответствии с законодательством, автомобили упомянутых категорий не могут выходить в рейс без установленного и активированного тахографа. Прибор должен быть исправен, не подвержен несанкционированным модификациям. Водитель не имеет права блокировать или отключать тахограф.
        </div>
        <div>
            <table class="pr">
                <tr>
                    <td>Штраф</td>
                    <td>Должностные лица</td>
                    <td>Водитель</td>
                </tr>
                <tr>
                    <td>Отсутствие тахографа</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Тахограф не работает</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Тахограф неисправен</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Тахограф блокирован</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Тахограф несанкционированно модифицирован</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Зафиксировано нарушение водителем режима труда и отдыха</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>Нарушение водителем режима труда и отдыха</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
                <tr>
                    <td>За отсутствие карты водителя</td>
                    <td>5 000 – 10 000 р.</td>
                    <td>1 000 – 3 000 р.</td>
                </tr>
            </table>
        </div>
        <div class="forfeit_diagram"></div>
        <div class="forfeit_text_6">
            КАК ПРАВИЛЬНО ВЫСТРОИТЬ РЕЖИМ ТРУДА<br/>И ОТДЫХА ВОДИТЕЛЯ
        </div>
        <div class="forfeit_time"></div>
        <div class="forfeit_text_7">
            Ключевые требования, регламентирующие время вождения и времени 
            отдыха водителя, представлены в Директиве (ЕС) 561/2006 Европейского 
            Парламента и Совета от 15.03.06.
        </div>
        <div class="suka_2"></div>
        <div class="forfeit_warning_2"></div>
        <div class="forfeit_text_8">
            число аварий по вине водителей<br/> транспортных средств снизилось на 22%!
        </div>
        <div class="forfeit_warning_3"></div>
        <div class="forfeit_text_9">
            Соблюдение данных норм позволит вам не только избежать штрафов, но и обезопасить себя 
            на дороге, ведь с момента введения требований к режиму труда и отдыха водителей и 
            контролирующих их соблюдение устройств (тахографов) число аварий по вине водителей 
            транспортных средств снизилось на 22%!
        </div>
    </div>
    
    
      
    <!-- Index content END -->
</div>
<!-- Index FOOTER START -->