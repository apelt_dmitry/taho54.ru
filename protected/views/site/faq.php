
<div class="select_bgr_1">
    <div class="container ">
        <div class="title_faq">
            ВОПРОС - ОТВЕТ
        </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Index content START -->
    <div class="services_div">
        <img style="  float: left;  margin-right: 20px;" src="/img/services_img_1.png"/>
        <div class="blue fs_24 mp_b">Нужны ли тахографы?</div>
        <div class="fs_20 mp_b">На 1 мая 2015 года в Приказе Министерства Транспорта РФ №36 от от 13 февраля 2013 г.  выделены следующие категории транспортных средств обязательные к установке тахографоф до:</div>
        <div class="merc_box_2">
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png">
                <img style="margin-top: 36px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 35px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 59px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Транспортные средства, предназначенные для перевозки опасных грузов,<br/>
                максимальной массой свыше 3,5 тонн - до 1 апреля 2014 года;<br/>
                <span style="margin-top: 6px; display: block;">Автобусы, имеющие помимо места водителя более 9 (девяти) мест для сидения<br/></span>
                пассажиров - до 1 июля 2014 года;<br/>
                <span style="margin-top: 6px;display: block;">Грузовые автомобили, максимальной массой более 15 тонн, предназначенные<br/></span>
                (внимание! Вы можете ездить на нём только по городу) для междугородней перевозки<br/>
                грузов - до 1 июля 2014 года;<br/>
                <span style="margin-top: 6px;display: block;">Грузовые автомобили, максимальной массой более 12 тонн - до 1 сентября 2014;<br/></span>
                <span style="margin-top: 6px;display: block;">Грузовые автомобили с максимальной массой от 3,5 тонн, но не более 12 тонн - до 1<br/></span>
                апреля 2015 года;<br/>
            </div>
        </div>
            <div class="faq_line"></div>
            <div class="blue fs_24 mp_b " style="display: inline-block; text-transform: uppercase;">
                Какие тахографы можно и нужно ставить?
            </div>
            <div class="fs_20 mp_b" style="margin-top: 16px;">
                Какие тахографы можно и нужно ставить?
                Тахограф ЕСТР или европейский тахограф – это тахограф, который имеет международный сертификат соответствия, одобрение типа и удовлетворяет требованиям Международного соглашения ЕСТР. Практически вся европейская техника уже оборудована такими тахографами.<br/>
                Тахограф с СКЗИ – это тахограф, в котором установлен блок криптозащиты (блок СКЗИ), куда помещается и хранится информация, собранная тахографом. На все устройства, оборудованные подобными блоками криптозащиты, распространяется действие Закона о защите государственной тайны. Все работы, связанные с блоками криптозащиты, подлежат лицензированию ФСБ.
            </div>
            <div class="fs_20 mp_b blue" style="margin: 30px 0; text-transform: uppercase; text-align: center;">
                Компания "ТИМФОРС" рекомендует:<br/>
                для перевозок по территории РФ
            </div>
            <div style="float: left; position: relative;">
                <div class="fs_20 mp_b" style="text-align: center;">
                    Европейские машины - тахограф <br/>
                    (DTCO VDO 3283) c блоком СКЗИ (НКМ)
                </div>
                <img src="/img/bb_vdo.png" class="contintal_best_buy" style="top:75px;  left: 50px;">
                <img class="cont_img" style="z-index: 2; margin-left: 56px; margin-top: 92px;" src="/img/vdo_3283.png">
                <a href="#" onclick="$('#formDialog').dialog('open'); return false;" class="continental_button" style="top:90px;  left: 100px;"></a>
            </div>
            <div style="float: right; position: relative;">
                <div class="fs_20 mp_b" style="text-align: center;">
                    Российские машины - тахограф<br/>
                    (АТОЛ DRIVE5) c блоком СКЗИ (НКМ)
                </div>
                
                <img src="/img/best_buy.png" class="atol_best_buy" style="top:75px;  left: 40px;">
                <img class="atol_tah"  src="/img/atol_tah.png" style="z-index: 2; margin-left: 40px; margin-top: 50px;">
                <a href="#" onclick="$('#formDialog').dialog('open'); return false;" class="atol_button" style="top:75px;  left: 85px;"></a>
            </div>
            <div class="fs_20 mp_b blue" style="display: inline-block;  text-transform: uppercase;  text-align: center;  width: 100%;  margin-top: 40px;">
                Для международных грузоперевозок
            </div>
            <div class="fs_20 mp_b" style="text-align: center; margin: 20px 0;">
                Европейские и Российские машины - тахограф (VDO 1381) без блока СКЗИ (НКМ)
            </div>
            <div style="position: relative;">
                <img src="/img/best_buy_red.png" class="contintal_best_buy" style="  margin-left: 260px;">
                <img class="cont_img" style="z-index: 2;  margin-left: 260px;" src="/img/vdo_continental.png">
                <a href="#" onclick="$('#formDialog').dialog('open'); return false;" class="continental_button" style="  margin-left: 260px;"></a>
            </div>
            <div class="faq_line"></div>
            <div class="blue fs_24 mp_b " style="display: inline-block; text-transform: uppercase;">
                Если машина оформлена на меня и нет ИП, ООО и т.д. как быть?
            </div>
            <div class="fs_20 mp_r" style="margin-top: 16px;">
             Вы физическое лицо и оказываете коммерческие услуги для организаций, но не имеете своего ИП или юридического лица. В этом случае устанавливать тахограф нужно, но есть достаточно неприятный момент! При активации тахографа в СКЗИ заносится эксплуатирующая организация, т.е. та организация с которой у вас заключен договор. Если у вас установлен тахограф, и каким то образом сотрудник выяснит что активирован он для одной организации, а груз вы перевозите для другой, это будет считаться нарушением. В ходе телефонного разговора с сотрудником ФБУ «Росавтотранса» я получил такой комментарий: «При смене эксплуатирующей организации, нужно менять СКЗИ». Не очень то гуманно при стоимости работ почти 20 тысяч! При таком подходе рекомендуем Вам оформить ИП и избавиться от этих хлопот.
            </div>
            <div class="faq_line"></div>
            <div class="blue fs_24 mp_b " style="display: inline-block; text-transform: uppercase;">
                сколько стоит тахограф
            </div>
            <div class="fs_20 mp_b" style="margin-top: 16px;  margin-bottom: 10px;">
                Цена тахографа формируется и включает в себя следующее:
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png">
                <img style="margin-top: 11px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 10px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                <span style="margin-top: 16px;">Тахограф;<br/></span>
                <span style="margin-top: 16px;">Активация тахографа;<br/></span>
                <span style="margin-top: 16px;">Монтаж тахографа;<br/></span>
                <span style="margin-top: 16px;">Подключение к штатному датчику скорости;<br/></span>
                <span style="margin-top: 16px;">Калибровка тахографа + сертификат и наклейка на кузов транспортного средства;</span>
            </div>
            <div class="fs_20 mp_b" style="margin-top: 16px;  margin-bottom: 10px; display: inline-block;  width: 100%;">
                По необходимости требуется дополнительно:
            </div>
            <div class="merc_box_1_1">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 10px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                <span style="margin-top: 16px;">Датчик скорости (импульсный);<br/></span>
                <span style="margin-top: 16px;">Монтаж датчика скорости;<br/></span>
                <span style="margin-top: 16px;">Спидометр электронный;<br/></span>
                <span style="margin-top: 16px;">Установка спидометра;<br/></span>
                <span style="margin-top: 16px;">Короб для установки тахографа (1DIN)<br/></span>
                <span style="margin-top: 16px;">Система мониторинга транспорта АТОЛ Drive.</span>
            </div>
            <div class="faq_line"></div>
            <div class="blue fs_24 mp_b " style="display: inline-block; text-transform: uppercase;">
                Что делать со старыми  тахографами, штатными и<br/> тахографами без блока СКЗИ? 
            </div>
            <div class="fs_20 mp_r" style="margin-top: 16px;">
            В соответствии с регламентом в Приказе Министерства Транспорта РФ №36 от от 13 февраля 2013 г.   с 1 апреля 2015 г., тахографы без блока СКЗИ, устанавливать категорически запрещается. Ранее установленные тахографы, шайбовые, техрегламент и естр без лицензии на международные грузо перевозки, разрешается эксплуатировать до 2018 года.
            </div>
            <div class="fs_24 mp_b" style="margin-top: 16px;">
                ЭКСПЛУАТАЦИЯ ЯПОНСКИХ, КОРЕЙСКИХ, КИТАЙСКИХ ТАХОГРАФОВ НА ТЕРРИТОРИИ РФ – РАПРЕЩЕННА.
            </div>
            <div class="faq_line"></div>
            <div class="blue fs_24 mp_b " style="display: inline-block; text-transform: uppercase;">
                Как обмануть тахограф?
            </div>
            <div class="fs_24 mp_b" style="margin-top: 16px;text-transform: uppercase;">
                Ответ есть! ОБМАНУТЬ без выявления последствий цифровой тахограф НЕВОЗМОЖНО!  
            </div>
            <div class="fs_20 mp_b" style="margin-top: 16px;  margin-bottom: 10px; display: inline-block;  width: 100%;">
                Любое вмешательство:
            </div>
            <div class="merc_box_1_1">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 8px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 10px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                <span style="margin-top: 16px;">Отключение питания от аккумулятора;<br/></span>
                <span style="margin-top: 16px;">Отключение провода зажигания;<br/></span>
                <span style="margin-top: 16px;">Отключение датчика скорости;<br/></span>
                <span style="margin-top: 16px;">Установка магнита на датчик скорости;<br/></span>
                <span style="margin-top: 16px;">Все это будет записано и с легкостью выявлено инспектором при проведении проверки.<br/></span>
            </div>
            <div class="faq_line"></div>
            <img src="/img/forfeit_police.png" style="margin: 0 20px 0 0;float: left;  max-width: 355px;"/>
            <div class="mp_b fs_24 blue"style="margin-top: 20px;display: inline-block; text-transform: uppercase;">
                Штрафы и тахограф
            </div>
            <div class="mp_r fs_20">
                С 1 апреля 2014 года за управление транспортным средством для перевозки грузов или пассажиров без тахографа, а также за нарушение водителем режима труда и отдыха введен штраф. Соответствующие изменения затронули <span class="mp_b">ст. 11.23 КоАП РФ.</span> До 1 апреля действие этой статьи распространялось только на международные автомобильные перевозки.
            </div>
            <div class="mp_r fs_20"style="display: inline-block">
                Размер штрафа за отсутствие тахографа, а также за его нерабочее состояние, или нарушение правил его эксплуатации для граждан составляет от 1 тыс. до 3 тыс. руб., для должностных лиц – от 5 тыс. до 10 тыс. руб. <span class="mp_b">(ч. 1 ст. 11.23 КоАП РФ).</span>
                Нарушение лицом, управляющим транспортным средством для перевозки грузов и пассажиров, установленного режима труда и отдыха влечет наложение административного штрафа в размере от 1 тыс. до 3 тыс. руб. <span class="mp_b">(ч. 2 ст. 11.23 КоАП РФ).</span>
            </div>
            <div class="faq_line"></div>
            <div class="mp_b fs_24 blue" style="text-align: center; width: 97%; text-transform: uppercase;">
                Какие бывают карты для тахографа?
            </div>
            <div class="mp_b fs_24 " style="text-align: center; margin-top: 20px; margin-bottom: 30px;">
                КАРТЫ ЕСТР
            </div>
            <div class="select_map_3">
                <div class="select_text_5">
                    Карта водителя для <br/>
                    тахографа ЕСТР
                </div>
                <div class="select_text_6">
                    <div class="select_galka_3"></div>
                    <div class="select_text_div">
                        Индивидуальная карточка для автомобильного тахографа международного стандарта без блока СКЗИ. Обязательна для коммерческих перевозок за пределами России. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_3"></div>
                    <div class="select_text_div">
                        Изготавливается персонально для каждого водителя.
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_3"></div>
                    <div class="select_text_div">
                        Выдается юр.лицам и сотрудникам предприятий.
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 3 ), array('class' => 'select_button')) ?>
            </div>
            <div class="select_map_4">
                <div class="select_text_5">
                    Карта предприятия <br/>
                    ЕСТР  
                </div>
                <div class="select_text_6">
                    <div class="select_galka_4"></div>
                    <div class="select_text_div">
                        Карта для контроля работы установленных в компании тахографов ЕСТР. Позволяет просматривать, выгружать и печатать данные с тахографов уже установленных на иномарках. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_4"></div>
                    <div class="select_text_div">
                        Карта автопредприятия европейского стандарта не персонифицирована. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_4"></div>
                    <div class="select_text_div">
                        Выдается только юр.лицам. Для компании достаточно 1 карты.  
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_4"></div>
                    <div class="select_text_div">
                        Для удобства изготавливают несколько карт компании
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 4 ), array('class' => 'select_button')) ?>
            </div>
            <div class="mp_b fs_24 " style="text-align: center;  margin-top: 20px; margin-bottom: 30px; text-transform: uppercase;  width: 100%;  float: left;">
                Карты ТехРегламент 
            </div>
            <div class="select_map_6">
                <div class="select_text_5">
                    Карта предприятия <br/>
                    техрегламент
                </div>
                <div class="select_text_6">
                    <div class="select_galka_6"></div>
                    <div class="select_text_div">
                        Тахокарта для учета работы водителей на автомобилях, оборудованных отечественными тахографами в соответствии с техрегламентом. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_6"></div>
                    <div class="select_text_div">
                        Карта компании техрегламент подходит только для тахографов без блока СКЗИ. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_6"></div>
                    <div class="select_text_div">
                        Служит для учета режима работы и отдыха водителя со стороны компании. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_6"></div>
                    <div class="select_text_div">
                        Предоставляет доступ к данным тахографа.  
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 6 ), array('class' => 'select_button')) ?>
            </div>
            <div class="select_map_1">
                <div class="select_text_5">
                    Карта водителя <br/>
                    по техрегламенту 
                </div>
                <div class="select_text_6">
                    <div class="select_galka_1"></div>
                    <div class="select_text_div">
                        Российская тахографическая<br/>
                        карта для устройств без блока<br/>
                        СКЗИ, соответствующих<br/>
                        техническому регламенту.
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_1"></div>
                    <div class="select_text_div">
                        Оснащение автомобилей<br/>
                        тахографами без СКЗИ<br/>
                        прекращено, но заказать карту<br/>
                        тахографа на замену или<br/>
                        изготовить для нового<br/>
                        сотрудника вы можете в<br/>
                        Новосибирске в компании <br/>
                        Team Force
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 1 ), array('class' => 'select_button')) ?>
            </div>
            <div class="mp_b fs_24 " style="text-align: center;  margin-top: 20px; margin-bottom: 30px; text-transform: uppercase;  width: 100%;  float: left;">
                Карты СКЗИ
            </div>
            <div class="select_map_2">
                <div class="select_text_5">
                    Карта водителя <br/>
                    скзи
                </div>
                <div class="select_text_6">
                    <div class="select_galka_2"></div>
                    <div class="select_text_div">
                        Чип-карта для цифрового тахографа
                        с блоком криптографической
                        защиты. Предназначена для
                        шифрованной записи данных о
                        передвижении транспортного
                        средства и режиме работы и отдыха
                        водителя на территории
                        Российской Федерации.
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_2"></div>
                    <div class="select_text_div">
                        Ключ цифрового тахографа <br/>
                        выпускается под закаказ для<br/>
                        персонификации водителя.  
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 2 ), array('class' => 'select_button')) ?>
            </div>
            <div class="select_map_5">
                <div class="select_text_5">
                    Карта предприятия <br/>
                    СКЗИ 
                </div>
                <div class="select_text_6">
                    <div class="select_galka_5"></div>
                    <div class="select_text_div">
                        Чип-карта фирмы, предоставляет доступ к данным тахографов с блоком криптозащиты. Предназначена для выгрузки данных с российских устройств контроля с блоком СКЗИ.<br/>
                        Предназначена для просмотра данных тахографа, их распечатки. 
                    </div>
                </div>
                <div class="select_text_6">
                    <div class="select_galka_5"></div>
                    <div class="select_text_div">
                        Заказать карту предприятия СКЗИ можно в количестве 1 и более штук онлайн или в Новосибирске
                        в офисе «Тимфорс»
                    </div>
                </div>
                <?= BsHtml::link('Оформить', array('site/estr', 'type' => 5 ), array('class' => 'select_button')) ?>
            </div>
  </div>  
    <!-- Index content END -->
</div>
<!-- Index FOOTER START -->