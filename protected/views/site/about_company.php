<script>
    $(document).ready(function(){
       $('.main_footer').css('margin-top','0px');
    });
</script>
<div class="main_slider_top">
    <div class="about_slider">
        <div class="container ">
            <div class="telek">
                <div class="slider_care">
                    <div style="width:100%;">
                        <div class="slider_about_company">
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_1.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_2.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_3.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_4.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_5.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_6.jpg"/>
                            </div>
                            <div class="about_slide_image">
                                <img src="/img/slider/about_slide_7.jpg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>

<!-- Наши клиенты начало-->
<div class="about_line">
    <div class="container">
        <div class="kovichki"></div>
        <div class="about_company_text_1"> о нашей компании</div>
        <div class="about_company_text_2">...из небольшой компании занимающейся решениями на <br/>автотранспорте, компания создала многоцелевую структуру...</div>
    </div>
</div>
<div style="width: 100%; height: 2px; background-color: #d3d3d3;"></div><!-- Разделительная полоса -->
<!-- Наши клиенты конец-->
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <div style="    width: 856px;
                    padding-left: 48px;
                    padding-top: 29px;
                    float: right;
                    position: relative;">
        <div class="about_1">
            <img style="float: left" src="/img/tf_2.png" /> 
            <div class="about_company_text_3">Группа компаний ТИМФОРС, была основана более 4-х лет назад</div>
            <div class="about_company_text_4">За это короткое время, из небольшой компании занимающейся решениями на авто транспорте, компания создала многоцелевую структуру в которую вошли такие направления как:</div>
            <div class="about_company_text_5">
                <img style="  float: left;margin-top: 4px;margin-right: 8px;" src="/img/galochka_about.png"/>
                Тахографы<br/>
                Мониторинг автотранспорта и других подвижных объектов.<br/>
                Системы контроля на промышленных предпритиях<br/>
                Системы контроля в горнодобывающей промышленности.<br/>
            </div>
        </div>
        <div class="about_2" style=" margin-bottom: 50px;">
            <img style=" margin: 6px 10px 40px 0; float: left; "src="/img/about_img.jpg"/>
            <div class="text_about_1">
                За это время, наши решения показали свою высокую	эффективность, высокие результатыбыли достигнуты благодаря отвественному подходу к делу, высокой степени квалификации наших сотрудников и желания делать свою работу так чтобы не пришлось ее переделывать в дальнейшем.
                Направленние "Тахографы" закрывает потребность многих компаний по обязательному оснащению транспортных средств устройствами учета времени работы водителя. Согласно Приказа Министерства транспорта Российской Федерации от 21 августа 2013 года № 273 
                <br/>Наша компания уже успешно оснастила в соответсвии с данным приказом многие предприятия нашего города и области.
            </div>
            <div class="work">
                <div class="work_text">С НАМИ УЖЕ РАБОТАЮТ:</div>
                <img class="about_work" src="/img/about_work.png"/>
            </div>
            <div class="work_text" style="margin-bottom: 30px;">ДОКУМЕНТЫ:</div>
            <div style="float: left; width:377px;">
                <img style="" src="/img/sertifikat_1.png"/>
                <div class="text_about_bot" style="margin-top: 20px;margin-bottom: 30px">Сертификат ТФ АГМ Атол</div>
                <img style="" src="/img/sertifikat_4.png"/>
                <div class="text_about_bot" style="margin-top: 20px;margin-bottom: 30px">Представительство ТФ Штрих-М</div>
                <img style="" src="/img/sertifikat_7.png"/>
            </div>
            <div style="float: left; width:210px; margin-right: 10px;">
                <div class="text_about_bot" style="margin-top: 0px;margin-bottom: 10px">Сертификат <br/>соответствия</div>
                <img style="" src="/img/sertifikat_2.png"/>
                <div class="text_about_bot" style="margin-top: 10px;margin-bottom: 20px">Лицензия ФСБ ТФ</div>
                <img style="" src="/img/sertifikat_5.png"/>
                <div class="text_about_bot" style="margin-top: 10px;margin-bottom: 20px">Клеймо RUS355 219 <br/>от 22.02.2013</div>
                <img style="" src="/img/sertifikat_8.png"/>
            </div>
            <div style="float: left; width:210px; ">
                <div class="text_about_bot" style="margin-top: 0px;margin-bottom: 33px">ТК Сертификат 2015 ТФ</div>
                <img style="" src="/img/sertifikat_3.png"/>
                <div class="text_about_bot" style="margin-top: 10px;margin-bottom: 20px"></div>
                <img style="  margin-top: 37px;" src="/img/sertifikat_6.png"/>
                <div class="text_about_bot" style="margin-top: 8px;margin-bottom: 20px">Клеймо РФ0173 1108 <br/>от 18.06.2013-7.pdf</div>
                <img style="" src="/img/sertifikat_9.png"/>
            </div>
        </div>
        
    </div>
</div>



<script>
    $(document).ready(function () {
       $('.single-item').slick({
            dots: false,
            infinite: true,
            speed: 500,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $('.slider_about_company').slick({
            arrows: true,
            //fade: true,
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    });
</script>