
<div class="select_bgr_1">
    <div class="container ">
        <div class="mercury_title_line" style="margin-top: 64px;">
           Тахограф Atol Drive 5 с блоком СКЗИ
       </div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Mercury content START -->
    <div style="width: 795px; float: right; margin-top: 40px; position: relative;">
        
        <div style="float: left">
            <img src="/img/best_buy.png" class="atol_best_buy"/>
            <img class="atol_tah" style="z-index: 2;"src="/img/atol_tah.png"/>
            <a href="#" class="atol_button" onclick="$('#formDialog').dialog('open'); return false;"></a>
        </div>

        <div class="right_cont">
            Надежное решение для автотранспортных предприятий и частных перевозчиков. Тахограф сертифицирован и соответствует законам РФ и требованиям Минтранса, имеет блок криптографической защиты данных, бесперебоен в эксплуатации и отличается долгим сроком службы.
        </div>
        
        <div class="atol_text_box_1">
            Компания TeamForce является авторизованным центром продажи тахографов Атол в Новосибирске, поэтому, покупая Atol Драйв 5 с СКЗИ, вы получаете два года гарантийного обслуживания и тахограф на подмену в случае ремонта.
        </div>
        
        <div class="merc_box_2 mt22" >
            <div class="merc_text_blue">
               Функциональные особенности тахографа Атолл Drive5:
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png">
                <img style="margin-top: 32px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 5px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 6px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 7px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 9px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Имеет блок криптографической защиты (СКЗИ) и обеспечивает безопасность данных от взлома и <br/>   
                фальсификации<br/> 
                Полностью соответствует законодательству России и разрешен к установке на её территории.<br/> 
                Интуитивно понятное управление. Любой водитель легко освоит работу с тахографом <br/> 
                Устанавливается на иномарки и российские автомобили любых моделей, подключается к<br/> 
                Цифровым и аналоговым спидометрам <br/> 
                Удаленная выгрузка данных
            </div>
        </div>
        <div class="merc_box_2 mt14">
            <div class="merc_text_blue">
               Почему АТОЛ Драйв 5 – выгодное решение для бизнеса:
            </div>
            <div class="merc_box_1_1">
                <img src="/img/forfeit_galka.png">
                <img style="margin-top: 103px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 33px;" src="/img/forfeit_galka.png">
                <img style="margin-top: 106px;" src="/img/forfeit_galka.png">
            </div>
            <div class="merc_box_1_2 merc_text_grey">
                Вы экономите на сервисе тахографа<br/> 
                - повышенная механическая прочность;<br/> 
                - устойчивость к тряскам и вибрациям;<br/> 
                - легкое извлечение батареи для замены;<br/> 
                - замена СКЗИ без вскрытия устройства;<br/> 
                Не несете затраты связанные с простоем транспорта при поломке тахографа: мы предоставляем<br/> 
                тахограф на подмену, чтобы автомобиль продолжал совершать рейсы не сбиваясь с графика<br/> 
                Не требуется обучение:<br/> 
                - легкое управление тахографом для водителя и для сотрудников фирмы<br/> 
                - функция дистанционной выгрузки информации<br/> 
                - идентификация водителя производится по карте тахографа<br/> 
                - моментальная печать данных о вождении<br/> 
                Адаптируйте тахограф под ваши нужды!<br/> 
                - персонализация цвета и яркости дисплея;<br/> 
                - опция системы GPRS на 2 SIM-карты (позволяет выбирать выгодные тарифы и  оператора);<br/> 
                - разъем для подключения к тахографу систем мониторинга, контроля уровня топлива,<br/> 
                &nbsp;&nbsp;систему ЭРА-ГЛОНАСС<br/> 
            </div>
        </div>
        <div style="float: left; margin-top: 30px;">
            <table  class="table_merc" >
                <tr>
                    <td class="td_merc_title" colspan="10" >
                        Технические характеристики  
                    </td>
                </tr>
                <tr>
                    <td>Соответствие «Приказ Минтранс №36» (встроен блок СКЗИ,<br/>внесен в реестр тахографов ФБУ Росавтотранс)</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Разрешен к установке на ТС, предназначенные для <br/>перевозки опасных грузов</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Блок криптографической защиты (СКЗИ)</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Защита от блокировки карт</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Гарантийное обслуживание, мес.</td>
                    <td>24</td>
                </tr>
                <tr>
                    <td>Модули расширения функционала</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>USB-разъем</td>
                    <td>Да</td>
                </tr>
                <tr>
                    <td>Диапазон рабочих температур тахографа, °С</td>
                    <td>-40…+70</td>
                </tr>
                <tr>
                    <td>Потребляемый ток в режиме работы без печати, А</td>
                    <td>0,1</td>
                </tr>
                <tr>
                    <td>Максимальный потребляемый ток, А</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Напряжение питания, В</td>
                    <td>8…36</td>
                </tr>
                <tr>
                    <td>Потребляемый ток, мА, не более</td>
                    <td>100 (в режиме печати не<br/>более 3 А)</td>
                </tr>
                <tr>
                    <td>Хранение данных при отключенном внешнем питании, <br/>дней, не менее</td>
                    <td>365</td>
                </tr>
                <tr>
                    <td>Ежедневных записей об использовании ТС, шт, не менее</td>
                    <td>365</td>
                </tr>
                <tr>
                    <td>Скорость печати, мм/сек</td>
                    <td>до 100</td>
                </tr>
                <tr>
                    <td>Размеры термохимической бумаги: - ширина, мм<br/>- максимальный внешний диаметр рулона, мм</td>   
                    <td>58<br/>30</td>
                </tr>
                <tr>
                    <td>Ресурс головки принтера (печатаемых отчетов), км, не менее</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td>Ресурс каждой кнопки, нажатий, не менее</td>
                    <td>150 000</td>
                </tr>
                <tr>
                    <td>Место установки</td>
                    <td>Гнездо 1 DIN (ISO7736)</td>
                </tr>
                <tr>
                    <td>Габаритные размеры:</td>
                    <td>180×188×58 </td>
                </tr>
                <tr>
                    <td>Масса, кг, не более</td>
                    <td>1.1</td>
                </tr>
            </table>
        </div>
        
    </div>
    <!-- Mercury content END -->
</div>
<!-- Index FOOTER START -->