<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<script>
    $(document).ready(function(){
        $('.main_footer').css('margin-top','0px');
       
        $('input[name="rr"]').change(function(){
            if ( $(this).attr('id')=='r1' ) {
                $('#delivery').val('0');
            } else {
                $('#delivery').val('1');
            }
        });
       
    });
</script>
<div class="select_bgr_1">
    <div id="stepler_1" class="container ">
        <a href="#stepler_1" class="step_1_active step_support scrolling"></a>
        <a href="#stepler_2" class="step_2 step_support scrolling"></a>
        <a href="#stepler_3" class="step_3 step_support scrolling"></a>
        <a href="#stepler_4" class="step_4 step_support step_support_2 scrolling"></a>
    </div>   
</div>
<div class="container">
    <div>
        <div class="estr_warning estr_warning_sup_1"></div>
        <div class="estr_text_1">
            <span style="text-transform: uppercase">Для заказа карты водителя ЕСТР потребуется:</span><br/>
            • Подготовить фотографию водителя 3,5*4,5<br/>
            • Заполненное заявление на выдачу карты водителя ЕСТР с вклеенной фотографией (скан)<br/>
            • Скан паспорта водителя (1-ая стр. и страница с пропиской)<br/>
            • Скан лицевой и обратной стороны водительских прав<br/>
            • Скан страхового свидетельства водителя (СНИЛС)<br/>
        </div>
    </div>
    <div class="ll_2"></div>
    <div>
        <a href="/img/estr/new_2.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1 ">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;"src="/img/estr/new_2.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        <a href="/img/estr/new_3.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1 ">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;"src="/img/estr/new_3.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        
        <div class="estr_text_3">
            Скачайте бланк заявление на <br/>
            получение карты тахографа
        </div>
        <a href="/site/DownloadFile?name=estr.zip"class="estr_button_1">
            Скачать
        </a>
        <br/>
        <div class="estr_warning estr_warning_sup_2"></div>
        <div class="estr_text_4 magin_top_24">
            Заполните от руки черной<br/> 
            гелиевой ручкой печатными буквами 
        </div>
        <div class="estr_blue_arrow estr_warning_sup_3"></div>
        <div class="estr_text_4 magin_top_18">
            Для заполнения сверьтесь <br/>
            с образцом 
        </div>
    </div>
    <div style="margin-top: 30px"class="ll_2"></div>
    <div>
        <a href="/img/estr/sample_3.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;  margin-bottom: 50px;"src="/img/estr/sample_3.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        
        <div class="estr_text_3">
            Вклейте фотографию в заполненный бланк
        </div>
        <div class="estr_warning estr_warning_sup_4"></div>
            <div class="estr_text_4 magin_top_18">
                <span style="text-transform: uppercase;line-height: 36px;">Подготовьте фотографию с учетом требований: <br/></span>
                1) Черно-белое распечатанное фото водителя по плечи на белом <br/>
                фоне. Предпочтительно в строгой темной одежде, чтобы она<br/>
                контрастировала с фоном. Черты лица должны быть хорошо<br/>
                различимы (не слишком темная, что не видно глаз, и не слишком<br/>
                яркая, когда заместо лица светлый круг).<br/>
                2) По горизонтали голова должна находиться в центре снимка.<br/>
                3) Голова должна занимать 65-75% площади фото<br/>
                4) Величина фотографии: 3,5*4,5 см. <br/>
            </div>
    </div>
    <div style="margin-top: 60px;">
        <a href="/img/estr/sample_4.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;  "src="/img/estr/sample_4.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        
        <div class="estr_text_3">
            Распишитесь на заявлении 
        </div>
        <div class="estr_warning estr_warning_sup_3"></div>
            <div class="estr_text_4 magin_top_18">
                Роспись должна располагаться в центре отведенного поля <br/>
                и не выходить за его рамки.
            </div>
    </div>
    <div style="margin-top: 30px"class="ll_2"></div>
    <div style="margin-top: 60px;">
        <a href="/img/estr/new_2.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;  "src="/img/estr/new_2.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        <a href="/img/estr/new_1.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px; "src="/img/estr/new_1.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>
        
        <div class="estr_text_3">
            отсканируйте заполненное вами заявление<br/>
            (или сделайте фото заявления крупным планом <br/>
            на ровной поверхности) и загрузите на наш сайт<br/>
        </div>
        <div class="estr_button_2" onclick="$('#doc1').click();">
            Загрузить
        </div>
        <form>
            <input  style=" display: none;"type="file" name="doc1" id="doc1">
        </form>
        <div id="progress1" class="progress" style="width: 200px">
            <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
        </div>
        
        <div class="estr_button_2 added_button" onclick="$('#doc2').click();">
            Загрузить
        </div>
        <form>
            <input  style=" display: none;"type="file" name="doc2" id="doc2">
        </form>
        <div id="progress2" class="progress added_progress" style="width: 200px">
            <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
        </div>
        
        <br/>
        <div class="estr_warning estr_warning_sup_3"></div>
        <div class="estr_text_4 magin_top_34">
            Перед загрузкой свертесь с образцом
        </div>
    </div>
</div>
<div style="margin-top: 30px"class="estr_line_grey">
    <div class="container">
        <a href="#step_2"class="estr_button_3">
            Следующий шаг
        </a>
        <div class="estr_blue_arrow" style="margin: 38px; float: left;"></div>
        <div class="estr_text_5" style="margin-top: 38px;">
            нАЖМИТЕ НА КНОПКУ , ЧТО БЫ<br/>
            ПЕРЕЙТИ К СЛЕДУЮЩЕМУ ШАГУ
        </div>
    </div>
</div>
<div id="step_2" style="display: none;">
    <div class="select_bgr_1">
        <div id="stepler_2" class="container ">
            <a href="#stepler_1" class="step_1 step_support scrolling"></a>
            <a href="#stepler_2" class="step_2_active step_support scrolling"style="margin-top: 30px;"></a>
            <a href="#stepler_3" class="step_3 step_support scrolling"></a>
            <a href="#stepler_4" class="step_4 step_support step_support_2 scrolling"></a>
        </div>   
    </div>  
    <div class="container">
        <div class="estr_black_box" ></div>
        <div class="estr_text_6" style="margin-top: 60px;">
            Прикрепите отсканированные документы <br/>
            (или фотографии на ровной поверхности крупным планом):
        </div>
        <div style="margin-top: 30px"class="ll_2"></div>
        <a href="/img/estr/sample_5.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
                <div class="sample_estr_press_1 estr_text_2">Образец</div>
                <img style="width: 164px; "src="/img/estr/sample_5.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/>что бы увеличить</div>
            </div>
        </a>
        
        <div class="estr_text_3" style="  width: 70%;  float: left;">
           Копия паспорта водителя (первый разворот) 
        </div>
        <div class="estr_button_2" onclick="$('#doc3').click();">
            Загрузить
        </div>
        <form>
            <input  style=" display: none;"type="file" name="doc3" id="doc3">
        </form>
        <div id="progress3" class="progress" style="width: 200px">
            <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
        </div>
        <br/>
        <div class="estr_blue_arrow estr_warning_sup_3"></div>
        <div class="estr_text_4 magin_top_18">
            Перед загрузкой <br/>
            сверьтесь с образцом 
        </div>
    </div>
    <div class="container" style="margin-top: 30px">
        <a href="/img/estr/sample_6.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px;   margin: 50px 0px;"src="/img/estr/sample_6.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
            </div>
        </a>
    
        <div class="estr_text_3" style="  width: 70%;  float: left;">
           Копия паспорта водителя (прописка)  
        </div>
        <div class="estr_button_2"onclick="$('#doc4').click();">
            Загрузить
        </div>
        <form>
            <input  style=" display: none;"type="file" name="doc4" id="doc4">
        </form>
        <div id="progress4" class="progress" style="width: 200px">
            <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
        </div>
        <br/>
        <div class="estr_blue_arrow estr_warning_sup_3"></div>
        <div class="estr_text_4 magin_top_18">
            Перед загрузкой <br/>
            сверьтесь с образцом 
        </div>
    </div>
    <div style="margin-top: 30px"class="ll_2"></div>
    <div class="container">
        <a href="/img/estr/sample_7.jpg" class="gallery">
            <div class="sample_estr sample_estr_sup_1">
            <div class="sample_estr_press_1 estr_text_2">Образец</div>
            <img style="width: 164px; margin-bottom: 50px;"src="/img/estr/sample_7.jpg"/>
            <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
        </div>
        </a>

        <div class="estr_text_3" style="  width: 70%;  float: left;">
           Водительское удостоверение с лицевой стороны 
        </div>
        <div class="estr_button_2"onclick="$('#doc5').click();">
            Загрузить
        </div>
        <form>
            <input  style=" display: none;"type="file" name="doc5" id="doc5">
        </form>
        <div id="progress5" class="progress" style="width: 200px">
            <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
        </div>
        <br/>
        <div class="estr_blue_arrow estr_warning_sup_3"></div>
        <div class="estr_text_4 magin_top_18">
            Перед загрузкой <br/>
            сверьтесь с образцом 
        </div>
    </div>
    <div class="container"style="margin-top: 30px">
    <a href="/img/estr/sample_8.jpg" class="gallery">
        <div class="sample_estr sample_estr_sup_1">
        <div class="sample_estr_press_1 estr_text_2">Образец</div>
        <img style="width: 164px;   margin-bottom: 50px;"src="/img/estr/sample_8.jpg"/>
        <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
    </div>
    </a>
    
    <div class="estr_text_3" style="  width: 70%;  float: left;">
       Водительское удостоверение с обратной стороны 
    </div>
    <div class="estr_button_2"onclick="$('#doc6').click();">
        Загрузить
    </div>
    <form>
        <input  style=" display: none;"type="file" name="doc6" id="doc6">
    </form>
    <div id="progress6" class="progress" style="width: 200px">
        <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
    </div>
    <br/>
    <div class="estr_blue_arrow estr_warning_sup_3"></div>
    <div class="estr_text_4 magin_top_18">
        Перед загрузкой <br/>
        сверьтесь с образцом 
    </div>
    <div style="margin-top: 30px"class="ll_2"></div>
        <div class="container">
            <a href="/img/estr/sample_9.jpg" class="gallery">
                <div class="sample_estr sample_estr_sup_1">
                    <div class="sample_estr_press_1 estr_text_2">Образец</div>
                    <img style="width: 164px; margin-bottom: 50px;"src="/img/estr/sample_9.jpg"/>
                    <div class="sample_estr_press_2 estr_text_2">Нажмите<br/> что бы увеличить</div>
                </div>
            </a>

            <div class="estr_text_3" style="  width: 70%;  float: left;">
               Снилс водителя
            </div>
            <div class="estr_button_2"onclick="$('#doc7').click();">
                Загрузить
            </div>
            <form>
                <input  style=" display: none;"type="file" name="doc7" id="doc7">
            </form>
            <div id="progress7" class="progress" style="width: 200px">
                <div class="progress-bar progress-bar-warning progress-bar-striped active"></div>
            </div>
            <br/>
            <div class="estr_blue_arrow estr_warning_sup_3"></div>
            <div class="estr_text_4 magin_top_18">
                Перед загрузкой <br/>
                сверьтесь с образцом 
            </div>
        </div>
    </div>
    <div style="margin-top: 30px"class="estr_line_grey">
        <div class="container">
            <a href="#step_3" class="estr_button_3 scrolling" >
                Следующий шаг
            </a>
            <div class="estr_blue_arrow" style="margin: 38px; float: left;"></div>
            <div class="estr_text_5" style="margin-top: 38px;">
                НАЖМИТЕ НА КНОПКУ , ЧТО БЫ<br/>
                ПЕРЕЙТИ К СЛЕДУЮЩЕМУ ШАГУ
            </div>
        </div>
    </div>
</div>
<div id="step_3" style="display: none;">
    
    <div class="select_bgr_1">
        <div id="stepler_3"class="container ">
            <a href="#stepler_1" class="step_1 step_support scrolling"></a>
            <a href="#stepler_2" class="step_2 step_support scrolling"></a>
            <a href="#stepler_3" class="step_3_active step_support scrolling" style="margin-top: 34px;"></a>
            <a href="#stepler_4" class="step_4 step_support step_support_2 scrolling"></a>
        </div>   
    </div>
    <div class="container">
        <div class="estr_clock" ></div>
        <div class="estr_text_6" style="margin-top: 46px;">
            Срок изготовления карты водителя для цифрового тахографа <br/>
            с блоком СКЗИ составляет примерно 21 день 
        </div>
    </div>
    <div style="margin-top: 30px"class="ll_2"></div>
    <div class="container">
        <div class="estr_download"></div>
        <div class="estr_text_3" style="  width: 70%;  float: left;">
           Водительское удостоверение с обратной стороны 
        </div>
        <div class="estr_text_4" style="margin-top: 10px;">
            Выберите подходящий способ доставки:
        </div>
        <p style="margin-top: 10px;">
        <input checked type="radio" id="r1" name="rr">
        <label for="r1" class="estr_text_4"style="font-weight: 0;"><span></span>Забрать карту в офисе</label>
        </p>
        <p>
        <input type="radio" id="r2" name="rr">
        <label for="r2" class="estr_text_4" style="font-weight: 0;"><span></span>Заказать доставку за 200 руб.</label>
        </p>
    </div>
    <div style="margin-top: 30px"class="estr_line_grey">
        <div class="container">
            <div class="credit"></div>
            <div class="estr_blue_arrow" style="margin: 38px; float: left;"></div>
            <a href="#step_4" class="estr_button_3 scrolling" >
                Следующий шаг
            </a>
            <div class="estr_blue_arrow" style="margin: 38px; float: left;"></div>
            <div class="estr_text_5" style="margin-top: 38px;">
                НАЖМИТЕ НА КНОПКУ , ЧТО БЫ<br/>
                ПЕРЕЙТИ К СЛЕДУЮЩЕМУ ШАГУ
            </div>
        </div>
    </div>
</div>
<div id="step_4" style="display: none;">
    <div class="select_bgr_1">
        <div id="stepler_4"class="container ">
            <a href="#stepler_1" class="step_1 step_support scrolling"></a>
            <a href="#stepler_2" class="step_2 step_support scrolling"></a>
            <a href="#stepler_3" class="step_3 step_support scrolling" style="margin-top: 34px;"></a>
            <a href="#stepler_4" class="step_4_active step_support step_support_2 scrolling"></a>
        </div>   
    </div>
    <div class="container">
        <a href="#" class="estr_button_3 scrolling" onclick="$('#modalUserName').modal('show'); return false;" >
            Оплатить!
        </a>
    </div>
</div>
    

<div class="modal fade" id="modalUserName">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Переход к оплате</h4>
            </div>

            <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'action'=>array('site/goPay'),
                'id'=>'users-form',
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    /*'afterValidate'=>'js:function(form, data, hasError){
                        if ( !hasError ) {

                            //var submit = $(form).find("[type=\"submit\"]");
                            //submit.addClass("disabled").find("span").remove();
                            //submit.prepend("<i class=\"fa fa-refresh fa-spin\"></i> ");

                            $.ajax({
                                type: "POST",
                                url: form[0].action,
                                data: $(form).serialize(),
                                success: function(ret) {
                                    if ( ret==1 ) {
                                        //$("#modalUserName").modal("hide");
                                        //$("#modalUserName").remove();
                                        //$("#goLuck").click();
                                    } else {
                                        //alert("Неизвестная ошибка. Повторите позже или обратитесь в поддержку.");
                                        //location = location;
                                    }
                                }
                            });

                            //return false;
                        }
                    }',*/
                ),
                'htmlOptions'=>array(
                    //'enctype'=>'multipart/form-data',
                ),
            )); ?>
                <div class="modal-body">
                    <?= BsHtml::alert(BsHtml::ALERT_COLOR_INFO, 'Пожалуйста, укажите контактные данные.') ?>
                    <div class="form-group">
                        <?= $form->labelEx($users, 'name', array(
                            'class'=>'control-label col-lg-4',
                        )) ?>
                        <div class="col-lg-8">
                            <?= $form->textField($users, 'name',array('required'=>'required')) ?>
                            <?= $form->error($users, 'name') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $form->labelEx($users, 'phone', array(
                            'class'=>'control-label col-lg-4',
                        )) ?>
                        <div class="col-lg-8">
                            <?= $form->textField($users, 'phone',array('required'=>'required')) ?>
                            <?= $form->error($users, 'phone') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $form->labelEx($users, 'email', array(
                            'class'=>'control-label col-lg-4',
                        )) ?>
                        <div class="col-lg-8">
                            <?= $form->textField($users, 'email',array('required'=>'required')) ?>
                            <?= $form->error($users, 'email') ?>
                        </div>
                    </div>
                    <?= BsHtml::hiddenField('unicode',$users->unicode) ?>
                    <?= BsHtml::hiddenField('delivery',0) ?>
                </div>
                <div class="modal-footer">
                    <?= BsHtml::submitButton('Готово', array(
                        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                        'icon' => BsHtml::GLYPHICON_OK,
                    )) ?>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    $('a.scrolling').click(function(){
        var idscroll = $(this).attr('href');//получаем значение атрибута href
        $.scrollTo(idscroll, 1000);// перематываем до блока(1000 - это длительность 1 сек.)
        return false;
    });//end click
    $('a[href="#step_2"]').click(function(){
        if ( $.cookie('step<?= $_GET['type']?>') >= 2 ){
            $('#step_2').show();
            $.scrollTo('#step_2', 1000);
        }
        else{
            alert('Загрузите все фотографии!');
        }
       // $('div').scrollTo(idscroll, 1000);
        return false;
    });//end click
    $('a[href="#step_3"]').click(function(){
        console.log('1')
        if ( $.cookie('step<?= $_GET['type']?>') >= 7 ){
            $('#step_3').show();
            $.scrollTo('#step_3', 1000);
        }
        else{
            alert('Загрузите все фотографии!');
            console.log('1')
        }
       // $('div').scrollTo(idscroll, 1000);
        return false;
    });//end click
     $('a[href="#step_4"]').click(function(){
        if ( $.cookie('step<?= $_GET['type']?>') >= 7 ){
            $('#step_4').show();
            $.scrollTo('#step_4', 1000);
        }
        else{
            alert('Загрузите все фотографии!');
        }
       // $('div').scrollTo(idscroll, 1000);
        return false;
    });//end click
    
    
    
    
    
    
    
    
    
    $('#doc1').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress1 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc2').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress2 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc3').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress3 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc4').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress4 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc5').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress5 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc6').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress6 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
    $('#doc7').fileupload({
        url: '/site/upload?type=<?= $_GET['type'] ?>',
        dataType: 'json',
        done: function (e, data) {
            //выполнение после загрузки
            console.log(data)
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress7 .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    });
});
</script>