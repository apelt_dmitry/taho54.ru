
<div class="select_bgr_1">
    <div class="container ">
       <div class="services_header_line"></div>
    </div>   
</div>
<!-- Index (содержание главной) Start -->
<div class="container">
    <!-- Left Widget Start -->
    <div style="width: 284px;float: left;margin-top: 30px;">
        <!-- Callback widget Start -->
        <div class="call_me">
            <a href="#" class="call_me_maybee" onclick="$('#uptocall-mini-phone').click(); return false;"></a>
        </div>
        <!-- Callback widget End -->
        <!-- Form widget Start -->
        <div class="spec_form">
            <!--Начало формы отправки письма-->
            <?php $contactForm = new ContactForm; ?>

            <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('site/contact'),
                'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Хъюстон у нас проблемы!!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'name', array(
                'placeHolder'=>'Контактное лицо',
            )); ?>

            <?= $form->textFieldControlGroup($contactForm,'phone', array(
                'placeHolder'=>'Телефон',
            )); ?>

            <?=BsHtml::submitButton('', array(
                'class' => 'float_right',
            ))?>

            <?php $this->endWidget(); ?>
            <!--Конец формы отправки письма-->
        </div>
        <!-- Form widget End -->
        <!-- Otziv slider Start -->
        <div class="slider_3">
            <div style="width:100%;">
                <div class="otzivi">
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                    <div class="otziv">
                        <div class="name_otziv">
                            Александр Миллер
                        </div>
                        <div class="dolzjnost_otziv">
                            “Газпром” Директор
                        </div>
                        <div class="text_otziv">
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem 
                            Ipsum has been the industry's
                            standard dummy text ever since
                            the 1500s, when an unknown 
                            printer took a galley of type
                            and scrambled it to make a type
                            specimen book. It has survived
                            not only five 
                            centuries, but also the leap into
                            electronic typesetting, remaining
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Otziv slider End -->
        <!-- za4em??? Start -->
        <div class="za4em">
            <div class="za4em_text2">
                Зачем нужен тахограф? 
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>
                Экономия на штрафах ГАИ за отсутствие устройства <span style="font-weight: bold;">1000 -           10 000 руб</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение расзходов на ГСМ <span style="font-weight: bold;">до 15%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Снижение риска аварий по вине водителя на <span style="font-weight: bold;">22%</span>
            </div>
            <div class="za4em_text">
                <div class="galochka"></div>Мониторинг местоположения и работы автомобиля
            </div>
        </div>
        <!-- za4em??? END -->
    </div>
    <!-- Left Widget End -->
    <!-- Index content START -->
    <div class="services_div">
        <div class="services_div_2">
            <div class="services_title">
                <div class="services_icon_1"></div>
                <a name="nik1"></a>
                Установка
            </div>
            <div class="news_text">
                Установка тахографов осуществляется в<br/>
                специализированном боксе, который имеет все<br/>
                необходимое оборудование и инструменты.<br/>
                Удобные подъезды и транспортная развязка дают<br/>
                возможность в любое время дня свободно<br/>
                приехать в установочный центр. Специалисты <br/>
                компании имеют все необходимые<br/>
                разрешительные документы и опыт<br/>
            </div>
        </div>
        <div class="services_img_1"><image src="/img/services_img_1.png"/></div>
        <div class="news_line"></div>
    </div>
    <div class="services_div">
        <div class="services_div_2">
            <div class="services_title">
                <div class="services_icon_2"></div>
                <a name="nik3"></a>
                Калибровка
            </div>
            <div class="news_text">
                Для того, чтобы тахограф соотвествовал всем<br/>
                необходимым функциональным параметрам<br/>
                неоходимо провести несколько процедур,<br/>
                связанных с вводом его в эксплуатацию. Процедура<br/>
                калибровки необходима для корректировки<br/>
                измеряемых, идентификационных, ограничивающих<br/>
                параметров.<br/>
                Калибровка тахографа осуществляется методом<br/>
                фиксированной дистанции, ТС прогоняется по<br/>
                определенному отмеренному расстоянию,<br/>
                учитывая размеры колес и давление в шинах.<br/>
            </div>
        </div>
        <div class="services_img_2"><image src="/img/services_img_2.png"/></div>
        <a onclick="$('#formDialog').dialog('open'); return false;" href="#"class="news_button">
            Узнать стоимость
        </a>
        <div class="news_line"></div>
    </div>
    <div class="services_div">
        <div class="services_div_2">
            <div class="services_title">
                <div class="services_icon_3"></div>
                <a name="nik4"></a>
                Изготовление карт водителя
            </div>
            <div class="news_text">
                    Для того, чтобы активировать работу тахографа<br/>
                    необходима идентификационная карта водителя, в<br/>
                    которой содержится СМАРТ-ЧИП. В соответсвии с  <br/>
                    законодательством , водитель который осуществялет<br/>
                    международные перевозки транспортным средством,<br/>
                    обязан иметь карту водителя.<br/>
                    Карты водителя выдаются физическим лицам:<br/>
                    1.   имеющим водительское удостоверение на право<br/>
                    управления автотранспортным средством категорий<br/>
                    “С“,  “D“,  “E“;<br/>
                    2.   возраст которых должен составлять:<br/>
                    ​а)  для водителей, занятых на перевозках грузов: <br/>
                    —   автотранспортными средствами (включая<br/>
                    прицепы и полуприцепы), разрешенный  <br/>
                    максимальный вес которых не превышает <br/>
                    7,5 т  - не моложе 18 лет;<br/>
                    —   иными автотранспортными средствами -<br/>
                    не моложе 21 года;<br/>
                    б)  для водителей, занятых перевозками<br/>
                    пассажиров - не моложе 21 года.<br/>
                    При этом водители, осуществляющие перевозки<br/>
                    пассажиров должны иметь стаж работы не менее<br/>
                    одного года в качестве: водителя грузовых<br/>
                    автотранспортных средств, разрешенный<br/>
                    максимальный вес которых превышает 3,5 тонны,<br/>
                    либо  в качестве водителя автотранспортных<br/>
                    средств, предназначенных для перевозки пассажиров;<br/>
                    3.  не имеющим на момент подачи заявления<br/>
                    (в случае первичной выдачи карты) иной действующей<br/>
                    карты водителя, выданной Компетентным органом по<br/>
                    ЕСТР Российской Федерации или другой<br/>
                    договаривающейся стороной ЕСТР;<br/>
                    4.  проживающим на территории Российской Федерации<br/>
                    в течение не менее 185 дней каждого календарного<br/>
                    года.<br/>
            </div>
        </div>
        <div class="services_img_2"><image src="/img/services_img_3.png"/></div>
        <a onclick="$('#formDialog').dialog('open'); return false;" href="#" class="news_button">
            Узнать стоимость
        </a>
        <?= BsHtml::link('оформить онлайн', array('site/map_selection' ), array('class' => 'services_button')) ?>
        <div class="news_line"></div>
    </div>
    <div class="services_div">
        <div class="services_div_2">
            <div class="services_title">
                <div class="services_icon_4"></div>
                <a name="nik2"></a>
                Настройка
            </div>
            <div class="news_text">
                Настройка тахографа, один из необходимых этапов,<br/>
                который надо выполнить, чтобы ввести устройство в<br/>
                эксплуатацию.<br/>
                Настройка осуществляется путем прошивки<br/>
                устройства, установки необходимых парметров для<br/>
                правильной работы тахографа.<br/>
            </div>
        </div>
        <div class="services_img_3"><image src="/img/services_img_4.png"/></div>
        <a onclick="$('#formDialog').dialog('open'); return false;" href="#" class="news_button">
            Узнать стоимость
        </a>
        <div class="news_line"></div>
    </div>  
    <div class="services_div">
        <div class="services_div_2">
            <div class="services_title">
                <div class="services_icon_5"></div>
                <a name="nik5"></a>
                Техническое обслуживание
            </div>
            <div class="news_text">
                Любое техническое устройство в результате его<br/>
                эксплуатации по тем или иным причинам может<br/>
                выйти из строя, либо начать давать некоректные<br/>
                данные.<br/>
                Наши специалисты в короткие сроки могут выяснить<br/>
                причину и при наличии технической возможности ее<br/>
                устранить
            </div>
        </div>
        <div class="services_img_3"><image src="/img/services_img_5.png"/></div>
        <a onclick="$('#formDialog').dialog('open'); return false;" href="#"class="news_button">
            Узнать стоимость
        </a>
        <div class="news_line"></div>
    </div>   
    <!-- Index content END -->
</div>
<!-- Index FOOTER START -->