
<div class="select_bgr_1">
    <div class="container ">
       <div class="select_img_1"></div>
       <div class="select_text_1">
           Не знаете, как получить карту тахографа? Думаете это сложно и долго?
       </div>
       <div class="select_text_2">
            Оформляйте карту водителя, карту предприятия и карту мастерской <br/>
            не отходя от своего компьютера! 
       </div>
    </div>   
</div>
<div style="height: 176px;">
    <div class="container ">
       <div class="select_img_2"></div>
       <div class="select_text_3">
            Следуйте простым инструкциям, оплачивайте карту он-лайн и через несколько <br/>
            минут ваши документы будут отправлены для изготовления нужной вам карты <br/>
            в «Русавтокарт»
       </div>
    </div>   
</div>
<div class="select_line_top"></div>
<div style="height: 144px;">
    <div class="container big_line_select"></div>
</div>
<div class="select_line"></div>
<div class="select_bgr_3">
    <div class="container">
        <div class="select_text_4">
            Какую карту вы хотели бы оформить:
        </div>
        <div class="select_map_1">
            <div class="select_text_5">
                Карта водителя <br/>
                по техрегламенту 
            </div>
            <div class="select_text_6">
                <div class="select_galka_1"></div>
                <div class="select_text_div">
                    Российская тахографическая<br/>
                    карта для устройств без блока<br/>
                    СКЗИ, соответствующих<br/>
                    техническому регламенту.
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_1"></div>
                <div class="select_text_div">
                    Оснащение автомобилей<br/>
                    тахографами без СКЗИ<br/>
                    прекращено, но заказать карту<br/>
                    тахографа на замену или<br/>
                    изготовить для нового<br/>
                    сотрудника вы можете в<br/>
                    Новосибирске в компании <br/>
                    Team Force
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 1 ), array('class' => 'select_button')) ?>
        </div>
        <div class="select_map_2">
            <div class="select_text_5">
                Карта водителя <br/>
                скзи
            </div>
            <div class="select_text_6">
                <div class="select_galka_2"></div>
                <div class="select_text_div">
                    Чип-карта для цифрового тахографа
                    с блоком криптографической
                    защиты. Предназначена для
                    шифрованной записи данных о
                    передвижении транспортного
                    средства и режиме работы и отдыха
                    водителя на территории
                    Российской Федерации.
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_2"></div>
                <div class="select_text_div">
                    Ключ цифрового тахографа <br/>
                    выпускается под закаказ для<br/>
                    персонификации водителя.  
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 2 ), array('class' => 'select_button')) ?>
        </div>
        <div class="select_map_3">
            <div class="select_text_5">
                Карта водителя для <br/>
                тахографа ЕСТР
            </div>
            <div class="select_text_6">
                <div class="select_galka_3"></div>
                <div class="select_text_div">
                    Индивидуальная карточка для автомобильного тахографа международного стандарта без блока СКЗИ. Обязательна для коммерческих перевозок за пределами России. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_3"></div>
                <div class="select_text_div">
                    Изготавливается персонально для каждого водителя.
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_3"></div>
                <div class="select_text_div">
                    Выдается юр.лицам и сотрудникам предприятий.
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 3 ), array('class' => 'select_button')) ?>
        </div>
        <div class="select_map_4">
            <div class="select_text_5">
                Карта предприятия <br/>
                ЕСТР  
            </div>
            <div class="select_text_6">
                <div class="select_galka_4"></div>
                <div class="select_text_div">
                    Карта для контроля работы установленных в компании тахографов ЕСТР. Позволяет просматривать, выгружать и печатать данные с тахографов уже установленных на иномарках. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_4"></div>
                <div class="select_text_div">
                    Карта автопредприятия европейского стандарта не персонифицирована. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_4"></div>
                <div class="select_text_div">
                    Выдается только юр.лицам. Для компании достаточно 1 карты.  
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_4"></div>
                <div class="select_text_div">
                    Для удобства изготавливают несколько карт компании
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 4 ), array('class' => 'select_button')) ?>
        </div>
        <div class="select_map_5">
            <div class="select_text_5">
                Карта предприятия <br/>
                СКЗИ 
            </div>
            <div class="select_text_6">
                <div class="select_galka_5"></div>
                <div class="select_text_div">
                    Чип-карта фирмы, предоставляет доступ к данным тахографов с блоком криптозащиты. Предназначена для выгрузки данных с российских устройств контроля с блоком СКЗИ.<br/>
                    Предназначена для просмотра данных тахографа, их распечатки. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_5"></div>
                <div class="select_text_div">
                    Заказать карту предприятия СКЗИ можно в количестве 1 и более штук онлайн или в Новосибирске
                    в офисе «Тимфорс»
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 5 ), array('class' => 'select_button')) ?>
        </div>
        <div class="select_map_6">
            <div class="select_text_5">
                Карта предприятия <br/>
                техрегламент
            </div>
            <div class="select_text_6">
                <div class="select_galka_6"></div>
                <div class="select_text_div">
                    Тахокарта для учета работы водителей на автомобилях, оборудованных отечественными тахографами в соответствии с техрегламентом. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_6"></div>
                <div class="select_text_div">
                    Карта компании техрегламент подходит только для тахографов без блока СКЗИ. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_6"></div>
                <div class="select_text_div">
                    Служит для учета режима работы и отдыха водителя со стороны компании. 
                </div>
            </div>
            <div class="select_text_6">
                <div class="select_galka_6"></div>
                <div class="select_text_div">
                    Предоставляет доступ к данным тахографа.  
                </div>
            </div>
            <?= BsHtml::link('Оформить', array('site/estr', 'type' => 6 ), array('class' => 'select_button')) ?>
        </div>
    </div>
</div>
