<?php

class SiteController extends Controller
{
	public function actionIndex()
	{   
            $this->pageTitle = 'Главная';
            $this->render('index');
	}
        
        
        public function actionContacts()
	{   
            $this->pageTitle = 'Контакты';
            $this->render('contacts');
	}
        
        
        public function actionNews($id=null)
        {
            $model = News::model()->findAll();

            if($id===null){
                $this->render('news', array(
                    'model' => $model,
                ));
            } else{
                $model = $this->loadNews($id);
                $this->render('new', array(
                    'model' => $model,
                ));
            }

        }
        public function loadNews($id)
        {
            $model = News::model()->findByPk($id);
            if ( !$model ) {
                throw new CHttpException(404, 'Новость не найдена.');
            }
            return $model;
        }
        public function actionMap_selection()
	{   
            $this->pageTitle = 'Выбор карты';
            $this->render('map_selection');
	}
        
        public function actionServices()
	{   
            $this->pageTitle = 'Наши услуги';
            $this->render('services');
	}
        
        public function actionForfeit()
	{         
            $this->pageTitle = 'Штрафы';
            $this->render('forfeit');
	}
        
        public function actionAbout_company()
	{   
            $this->pageTitle = 'О компании';
            $this->render('about_company');
	}
        
        public function actionMercury()
	{   
            $this->pageTitle = 'Меркурий';
            $this->render('mercury');
	}
        
        public function actionContinental()
	{   
            $this->pageTitle = 'VDO Continental';
            $this->render('continental');
	}
        
        public function actionContinental1381()
	{   
            $this->pageTitle = 'Continental 1381';
            $this->render('continental1381');
	}
        
        public function actionDTCO3283()
	{   
            $this->pageTitle = 'DTCO3283';
            $this->render('DTCO3283');
	}
        
        public function actionAtol()
        {   
            $this->pageTitle = 'Атол';
            $this->render('atol');
        }
        
        public function actionCatalog()
	{   
            $this->pageTitle = 'Каталог';
            $this->render('catalog');
	}
        
        public function actionSpeedometers()
	{   
            $this->pageTitle = 'Спидометры';
            $this->render('speedometers');
	}
        public function actionDatchiki()
	{   
            $this->pageTitle = 'Датчики';
            $this->render('datchiki');
	}
        public function actionFaq()
	{   
            $this->pageTitle = 'Вопрос - Ответ';
            $this->render('faq');
	}
        
        
        
        public function actionUpload()
        {   
            if( strlen($_FILES['doc1']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc1']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc1']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc1']['tmp_name'], './uploads/doc/'.$fileName);
                
                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                )); 
                if(strlen($users->blank_1) == 0 ){
                    $users->step += 1;
                }
                $users->blank_1 = $fileName;
                                
                $users->update(array(
                    'blank_1',
                    'step'
                ));
                
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
                
            }
            
            if( strlen($_FILES['doc2']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc2']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc2']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc2']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_2) == 0 ){
                    $users->step += 1;
                }
                $users->blank_2 = $fileName;
                
                $users->update(array(
                    'blank_2',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc3']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc3']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc3']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc3']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));  
                
                if(strlen($users->blank_3) == 0 ){
                    $users->step += 1;
                }
                $users->blank_3 = $fileName;
                $users->update(array(
                    'blank_3',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc4']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc4']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc4']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc4']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_4) == 0 ){
                    $users->step += 1;
                }
                $users->blank_4 = $fileName;
                
                $users->update(array(
                    'blank_4',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc5']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc5']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc5']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc5']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_5) == 0 ){
                    $users->step += 1;
                }
                $users->blank_5 = $fileName;
                
                $users->update(array(
                    'blank_5',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc6']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc6']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc6']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc6']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_6) == 0 ){
                    $users->step += 1;
                }
                $users->blank_6 = $fileName;
                
                $users->update(array(
                    'blank_6',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc7']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc7']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc7']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc7']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_7) == 0 ){
                    $users->step += 1;
                }
                $users->blank_7 = $fileName;
                
                $users->update(array(
                    'blank_7',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
            if( strlen($_FILES['doc8']['tmp_name']) != 0 ){
                $fileExt = pathinfo($_FILES['doc8']['name'], PATHINFO_EXTENSION);
                $fileName = substr(md5($_FILES['doc8']['name'].microtime()), 0, 28).'.'.$fileExt;
                copy($_FILES['doc8']['tmp_name'], './uploads/doc/'.$fileName);

                $request = Yii::app()->request;
                $type=$request->getParam('type');
                $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));
                if(strlen($users->blank_8) == 0 ){
                    $users->step += 1;
                }
                $users->blank_8 = $fileName;
                
                $users->update(array(
                    'blank_8',
                    'step'
                ));
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, $users->step, array('path'=>'/', 'expire'=>time()+3600*24*30));
            }
            
        }

        public function actionApp()
	{   
            include_once 'app.php';
	}
        
        public function actionEstr($type)
	{   
            $config = $this->config;
            $this->pageTitle = 'Оформление карты';
            
            switch ($type) {
                case 1:
                    $a = 'tr';
                    break;
                case 2:
                    $a = 'sczi';
                    break;
                case 3:
                    $a = 'estr';
                    break;
                case 4:
                    $a = 'company_estr';
                    break;
                case 5:
                    $a = 'company_sczi';
                    break;
                case 6:
                   $a = 'company_tr';
                    break;
            }
                   
            
            $request = Yii::app()->request;
            $users = Users::model()->findByAttributes(array(
                    'unicode' => $request->cookies['unicode']->value,
                    'type' => $type,
                ));  
            if( $request->cookies['unicode']->value === null || $request->cookies['step'.$type] === null || !$users ){
               // if( $request->cookies['unicode']->value === null ){
                    $unicode = md5($_SERVER['remote_addr'].microtime());
                    $request->cookies['unicode'] = new CHttpCookie('unicode', $unicode, array('path'=>'/', 'expire'=>time()+3600*24*30));
                //}
                $request->cookies['step'.$type] = new CHttpCookie('step'.$type, 0, array('path'=>'/', 'expire'=>time()+3600*24*30));
                $users = new Users();
                $users->type = $type;
                $users->step = 0;
                $users->unicode = $unicode;
                $users->price = $config->{'price_'.$type};
                $users->save();
                //print_r($users->errors);
            } else {
                
                //echo $users->unicode;
            }
            
            
            
            $this->render($a,array(
                'users' => $users,
            ));

	}
        
        
        public function actionGoPay()
        {      
            $users = Users::model()->findByAttributes(array(
                'unicode' => $_POST['unicode'],
            ));  
            if( !$users ){
                $this->redirect('/');
            }
            
            //$users->attributes = $_POST['Users'];
            $users->delivery = $_POST['delivery'];
            $users->name = $_POST['Users']['name'];
            $users->phone = $_POST['Users']['phone'];
            $users->email = $_POST['Users']['email'];
            
//            if ( $users->delivery ) {
//                $users->price += 200;
//            }
            
            $users->save();
            
            Yii::app()->robokassa->pay(
                $users->price + ( ($users->delivery) ? 200 : 0 ),
                $users->id,
                'Оплата карты тахографа',
                $users->email
            );
        }
        
        public function actionPayment()
        {
            $rc = Yii::app()->robokassa;

            $rc->onSuccess = function($event){
                $transaction = Yii::app()->db->beginTransaction();
                $InvId = Yii::app()->request->getParam('InvId');
                $payment = Users::model()->findByPk($InvId);
                $payment->isPayment = 1;
                $payment->datePayment = time();
                $payment->update(array('isPayment', 'datePayment'));
                $transaction->commit();

//                include_once "libmail.php";
//                $m = new Mail;
//                $m->From($_SERVER['SERVER_NAME'].';admin@kadastr-info.ru');
//                $m->To($payment->email);
//                $m->Subject('Информация, кадастровый номер: '.$payment->num);
//                $m->Body('Здравствуйте.<br />
//                        Вы купили информацию об участке с кадастровым номером <b>'.$payment->num.'</b>.<br />
//                        <b>Ссылка на полную информацию: <a href="'.Yii::app()->createAbsoluteUrl('site/index', array('hash'=>$payment->hash)).'">'.Yii::app()->createAbsoluteUrl('site/index', array('hash'=>$payment->hash)).'</a></b>
//                ', 'html');
//                $m->Send();
            };

            $rc->onFail = function($event){
                $InvId = Yii::app()->request->getParam('InvId');
                //Users::model()->findByPk($InvId)->delete();
            };

            $rc->result();
        }
        public function actionPaymentSuccess()
        {
//            $InvId = Yii::app()->request->getParam('InvId');
//            $payment = Users::model()->findByPk($InvId);
//            if ( $payment ) {
//                $this->redirect(array('site/index', 'hash'=>$payment->hash));
//            } else {
//                $this->redirect('/');
//            }
            $this->redirect('/');
        }
        public function actionPaymentFail()
        {
            $this->redirect('/');
        }
        
        public function actionDownloadFile($name)
        {      
            $file = "./uploads/uploaded_document/".$name; // " ./img "
            // отдаем файл
            Yii::app()->request->sendFile(basename($file),file_get_contents($file));
        }
     
        public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
        
        public function actionContact()
        {
            $model = new ContactForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='contact-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['ContactForm']) ) {
                $model->attributes = $_POST['ContactForm'];
                if ( $model->validate() ) {
                    $model->goContact();
                    echo '1';
                } else {
                    echo '0';
                    print_r($model->errors);
                }
                //Yii::app()->end();
            }
            //$this->redirect(array('site/index'));
        }
        
        public function actionOrder()
        {
            $model = new OrderForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='order-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['OrderForm']) ) {
                $model->attributes = $_POST['OrderForm'];
                if ( $model->validate() ) {
                    $model->goOrder();
                    echo '1';
                } else {
                    echo '0';
                    print_r($model->errors);
                }
                Yii::app()->end();
            }
            $this->redirect(array('site/index'));
        }
}